<?php

use App\Http\Controllers\Tetapan\BajetController;
use App\Http\Controllers\Tetapan\DunController;
use App\Http\Controllers\Tetapan\ModuleController;
use App\Http\Controllers\Tetapan\PenggunaController;
use App\Http\Controllers\Tetapan\RefDaerahController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Tetapan\RoleController;
use App\Http\Controllers\Tetapan\RoutesController;

/*
|--------------------------------------------------------------------------
| Tetapan Routes (for tetapan only)
|--------------------------------------------------------------------------
|
| Here is where you can register tetapan routes for MAIM application.
|
*/

//tetapan route

Route::group([
    'prefix' => 'tetapan',
    // 'middleware' => ['auth']
],
function () {

    Route::resources([
        'Pengguna' => PenggunaController::class,
        'Role' => RoleController::class,
        'Routes' => RoutesController::class,
        'Module' => ModuleController::class,
    ]);

    //module
    Route::get('module-dt/{ModuleId}', [ModuleController::class, 'RouteDt'])->name('Module-dt');
    Route::get('route-list/{ModuleId}', [ModuleController::class, 'routeList'])->name('route-list');
    Route::get('hapus-route-list/{ModuleId}', [ModuleController::class, 'hapusRouteList'])->name('hapus-route-list');
    Route::post('add-module-route', [ModuleController::class, 'storeModuleRoute'])->name('add-module-route');
    Route::post('hapus-module-route', [ModuleController::class, 'hapusModuleRoute'])->name('hapus-module-route');

    //role
    Route::get('create-role', [RoleController::class, 'createRole'])->name('create-role');

    //profile pengguna
    Route::get('profil/{id}', [PenggunaController::class, 'profile'])->name('profil');
    Route::put('profil/updateProfile/{id}', [PenggunaController::class, 'updateProfile'])->name('profil.updateProfile');
    Route::get('Pengguna/reset-password/{id}', [PenggunaController::class, 'resetPassword'])->name('reset-password');

    //permohonan asnaf
    Route::get('daerah/{negeriId}', [RefDaerahController::class, 'getDaerah'])->name('getDaerah');
});
