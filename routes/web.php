<?php

use App\Http\Controllers\Admin\Asnaf\SemakanPermohonanAsnafController;
use App\Http\Controllers\PermohonanAkaunController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * landing page routes
 */
Route::get('/', [App\Http\Controllers\Landing\LandingController::class, 'index'])->name('landing.index');


Auth::routes();

Route::prefix('asnaf')->group(function () {
    Route::resource('permohonan-akaun', PermohonanAkaunController::class);
});

Route::group([
    'middleware' => ['auth']
], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth', ]
], function () {
    Route::resource('semakan-permohonan-asnaf', SemakanPermohonanAsnafController::class);
});


