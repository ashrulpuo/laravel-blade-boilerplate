
const mix = require('laravel-mix');
let WebpackRTLPlugin = require('webpack-rtl-plugin');
const glob = require('glob');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/appv2.js', 'public/js')
    .sourceMaps();

/**
 * login bundle
 */
mix.styles([
    'resources/css/style.css',
    'resources/js/vendors/core/core.css',
    'resources/fonts/feather-font/css/iconfont.css',
    'resources/js/vendors/flag-icon-css/css/flag-icon.min.css'
], 'public/asset/login/login.bundle.css');


/**
 * backend bundle
 */
mix.styles([
    'resources/js/vendors/flag-icon-css/css/flag-icon.min.css',
    'resources/fonts/feather-font/css/iconfont.css',
    'resources/js/vendors/flatpickr/flatpickr.min.css',
    'resources/css/style.css',
    'resources/js/vendors/core/core.css',
    'resources/js/vendors/datatables.net-bs5/dataTables.bootstrap5.css',
    'resources/js/vendors/sweetalert2/sweetalert2.min.css'
], 'public/asset/backend/backend.bundle.css');

// mix.scripts([
//     'node_modules/feather-icons/dist/feather.js',
//     'node_modules/feather-icons/dist/feather.min.js',
// ], 'public/asset/backend/feather-font.bundle.js');

mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/jquery/dist/jquery.min.js',
    'resources/js/vendors/core/core.js',
    'resources/js/vendors/flatpickr/flatpickr.min.js',
    'resources/js/vendors/feather-icons/feather.min.js',
    'resources/js/dist/template.js',
    'resources/js/vendors/apexcharts/apexcharts.min.js',
    'node_modules/jquery.repeater/jquery.repeater.js',
    'node_modules/jquery.repeater/jquery.repeater.min.js',
    'resources/js/vendors/sweetalert2/sweetalert2.min.js',
    'resources/js/vendors/jquery-validation/jquery.validate.min.js'
], 'public/asset/backend/backend.bundle.js');

/**
 * datatable bundle
 */
mix.styles([
    'resources/js/vendors/datatables.net-bs5/dataTables.bootstrap5.css',
], 'public/asset/backend/datatables.bundle.css');

mix.styles([
    'resources/js/vendors/datatables.net/jquery.dataTables.js',
    'resources/js/vendors/datatables.net-bs5/dataTables.bootstrap5.js',
], 'public/asset/backend/datatables.bundle.js');

/**
 * jquery form repeater bundle
 */
mix.scripts([
    'node_modules/jquery.repeater/jquery-1.11.1.js',
    'node_modules/jquery.repeater/jquery.repeater.js',
    'node_modules/jquery.repeater/jquery.repeater.min.js',
], 'public/asset/backend/repeater.bundle.js');


/**
 * other
 */

(glob.sync('resources/js/function/**/*.js!(.nc)') || []).forEach(file => {
    mix.js(file, `public/${file.replace('resources/', '').replace('.js', '.js')}`);
});

(glob.sync('resources/js/tetapan/*.js!(.nc)') || []).forEach(file => {
    mix.js(file, `public/${file.replace('resources/', '').replace('.js', '.js')}`);
});

mix.copyDirectory('resources/landing', 'public/landing');


mix.styles([
    'resources/js/vendors/flag-icon-css/flags/4x3/my.svg',
], 'public/asset/flags/4x3/my.svg');

mix.sourceMaps(true, 'source-map')
  .webpackConfig({
    plugins: [
      new WebpackRTLPlugin()
    ]
})
    .browserSync({
        proxy: '127.0.0.1:8000',
        port: 3100,
        ghostMode: false,
        notify: false
    })

    .copyDirectory([
        'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'
    ],'public/asset/plugins/bootstrap-datepicker')

    .copyDirectory([
        'node_modules/fullcalendar/main.min.js',
        'node_modules/fullcalendar/main.min.css'
    ], 'public/asset/plugins/fullcalendar')

    .copyDirectory('node_modules/moment/min/moment.min.js', 'public/asset/plugins/moment/moment.min.js')

    .copyDirectory([
        'node_modules/dropzone/dist/min/dropzone.min.js',
        'node_modules/dropzone/dist/min/dropzone.min.css'
    ] , 'public/asset/plugins/dropzone')


    .copyDirectory([
        'resources/js/sweet.js',
    ] , 'public/js/sweet.js')
    .copyDirectory([
        'node_modules/select2/dist/js/select2.min.js',
        'node_modules/select2/dist/css/select2.min.css'
    ] , 'public/asset/plugins/select2')
    .copyDirectory('node_modules/jquery-validation/dist/jquery.validate.min.js', 'public/asset/plugins/jquery-validation/jquery.validate.min.js')
    // .copyDirectory([
    //     'resources/js/function/*/*',
    // ] , 'public/function')


