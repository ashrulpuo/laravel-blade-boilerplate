import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'node_modules/jquery/dist/jquery.js',
                'node_modules/jquery/dist/jquery.min.js',
                'resources/sass/app.scss',
                'resources/js/app.js',
                'resources/css/style-rtl.css',
                'resources/css/style-rtl.min.css',
                'resources/css/style.css',
                'resources/css/style.min.css',
                'resources/fonts/feather-font/css/iconfont.css',
                'resources/js/vendors/core/core.css',
                'resources/js/vendors/flatpickr/flatpickr.min.css',
                'resources/js/vendors/flag-icon-css/css/flag-icon.min.css',
                'resources/js/dist/template.js',
                'resources/js/vendors/core/core.js',
                'resources/js/dist/dashboard-light.js',
                'resources/js/vendors/apexcharts/apexcharts.min.js',
                'resources/js/vendors/flatpickr/flatpickr.min.js',
                'resources/js/vendors/feather-icons/feather.min.js',
                'node_modules/jquery.repeater/jquery.repeater.js',
                'node_modules/jquery.repeater/jquery.repeater.min.js',
                'resources/js/vendors/datatables.net-bs5/dataTables.bootstrap5.css',
                'resources/js/vendors/sweetalert2/sweetalert2.min.css',
                'resources/js/vendors/datatables.net/jquery.dataTables.js',
                'resources/js/vendors/datatables.net-bs5/dataTables.bootstrap5.js',
                'resources/js/vendors/sweetalert2/sweetalert2.min.js'
            ],
            refresh: true,
        }),
    ],
});
