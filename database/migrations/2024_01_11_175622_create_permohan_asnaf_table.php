<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PermohonanAsnaf', function (Blueprint $table) {
            $table->uuid('PermohonanAsnafId')->primary();

            $table->string('Nama');
            $table->string('NoKp');
            $table->string('Email');
            $table->string('NoTel');
            $table->integer('Jantina');
            $table->integer('StatusPerkahwinan');
            $table->string('Alamat');
            $table->uuid('NegeriId');
            $table->uuid('DaerahId');
            $table->integer('Poskod');
            $table->string('Kariah');
            $table->integer('Status')->default(1);
            $table->uuid('PegawaiSemak')->nullable();
            $table->longText('Catatan')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PermohonanAsnaf');
    }
};
