<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Routes', function (Blueprint $table) {
            $table->uuid('RoutesId')->primary();
            $table->string('Method');
            $table->string('Uri');
            $table->string('Name');
            $table->string('Controller');
            $table->string('Function');
            $table->string('Namespace')->nullable();
            $table->string('Hash');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Routes');
    }
};
