<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Traits\Uuids;

return new class extends Migration
{
    use Uuids;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MaklumatPengguna', function (Blueprint $table) {
            $table->uuid('MaklumatPenggunaId')->primary();
            $table->uuid('PenggunaId');
            $table->uuid('BahagianId')->nullable();
            $table->uuid('JawatanId')->nullable();
            $table->string('NoTel')->nullable();
            $table->string('NoTelPejabat')->nullable();
            $table->integer('Penyiasat')->nullable()->default(0);
            $table->integer('Pentadbir')->default(0);
            $table->integer('Hapus')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MaklumatPengguna');
    }
};
