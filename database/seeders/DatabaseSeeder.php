<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\MaklumatPengguna;
use App\Models\RefJawatan;
use Database\Seeders\RefbahagianSeeder;
use Database\Seeders\RefKategoriSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            UserSeeder::class,
            RefNegeri::class,
            RefDaerah::class,
        ]);
    }
}
