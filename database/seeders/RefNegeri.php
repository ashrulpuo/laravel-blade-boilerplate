<?php

namespace Database\Seeders;

use App\Models\RefNegeri as ModelsRefNegeri;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefNegeri extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $Negeri = [
            [
                "RefNegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "Negeri" => "Johor"
            ],
            [
                "RefNegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "Negeri" => "Kedah"
            ],
            [
                "RefNegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "Negeri" => "Kelantan"
            ],
            [
                "RefNegeriId" => "994DADA4-23BA-46BD-B561-8BC15F486091",
                "Negeri" => "Melaka"
            ],
            [
                "RefNegeriId" => "994DADA4-24BD-4083-81BC-52332806B322",
                "Negeri" => "Negeri Sembilan"
            ],
            [
                "RefNegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "Negeri" => "Pahang"
            ],
            [
                "RefNegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "Negeri" => "Perak"
            ],
            [
                "RefNegeriId" => "994DADA4-2718-4246-8BD0-039B667D68D6",
                "Negeri" => "Perlis"
            ],
            [
                "RefNegeriId" => "994DADA4-27B5-41F3-8E4E-47898F66FB0D",
                "Negeri" => "Pulau Pinang"
            ],
            [
                "RefNegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "Negeri" => "Sabah"
            ],
            [
                "RefNegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "Negeri" => "Sarawak"
            ],
            [
                "RefNegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "Negeri" => "Selangor"
            ],
            [
                "RefNegeriId" => "994DADA4-2B41-4863-9509-7320773682FF",
                "Negeri" => "Terengganu"
            ],
            [
                "RefNegeriId" => "994DADA4-2BED-4F81-B3CB-E877C5EDD2C5",
                "Negeri" => "Wilayah Persekutuan"
            ]
        ];

        foreach ($Negeri as $key => $negeri) {
            ModelsRefNegeri::create([
                "RefNegeriId" => $negeri["RefNegeriId"],
                "Negeri" => $negeri["Negeri"],
                "created_at" => now(),
                "updated_at" => now()
            ]);
        }

    }
}
