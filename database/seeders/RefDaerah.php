<?php

namespace Database\Seeders;

use App\Models\RefDaerah as ModelsRefDaerah;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RefDaerah extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Daerah = array(
            array(
                "DaerahId" => "994DB530-6665-4100-9865-EF248FA880DC",
                "Name" => "Batu Pahat",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:53.000Z",
                "updated_at" => "2023-06-01T03:31:53.000Z"
            ),
            array(
                "DaerahId" => "994DB530-70F5-4C06-BA31-8192DD1CC9FC",
                "Name" => "Johor Bahru",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:53.000Z",
                "updated_at" => "2023-06-01T03:31:53.000Z"
            ),
            array(
                "DaerahId" => "994DB530-8C18-45A5-B90B-CF1043873981",
                "Name" => "Kluang",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:53.000Z",
                "updated_at" => "2023-06-01T03:31:53.000Z"
            ),
            array(
                "DaerahId" => "994DB530-9F0B-49AE-80B6-DAC550C1DDD2",
                "Name" => "Kota Tinggi",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:53.000Z",
                "updated_at" => "2023-06-01T03:31:53.000Z"
            ),
            array(
                "DaerahId" => "994DB530-B566-4A07-A9A2-3BED27CDBF42",
                "Name" => "Mersing",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:53.000Z",
                "updated_at" => "2023-06-01T03:31:53.000Z"
            ),
            array(
                "DaerahId" => "994DB530-D8D0-4DE1-8DC6-90984A180B9B",
                "Name" => "Pontian",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB530-DA27-42A5-BC99-419FCBD8D33F",
                "Name" => "Muar",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB530-DB39-47B4-AB58-E63AE8835AD2",
                "Name" => "Segamat",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB530-DD87-4A41-B95E-7AD18BA5447F",
                "Name" => "Kulai",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB530-DE99-43C3-893D-BF3FEE384396",
                "Name" => "Tangkak",
                "NegeriId" => "994DADA4-195D-46FF-B656-A39C0ECEAFD2",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB530-DFF1-45E9-99F0-9BF4232BF354",
                "Name" => "Baling",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB530-E7CB-46E3-AF57-A5A8C24EEE41",
                "Name" => "Bandar Baharu",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB530-EC8F-4CE0-92ED-E57096BF0F54",
                "Name" => "Kota Setar",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB530-F3EA-4D73-9F60-843A1BDE4E3A",
                "Name" => "Kuala Muda",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB530-F639-41BF-B130-ACC290ABE271",
                "Name" => "Kubang Pasu",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-039D-4B34-B3CA-168CA96C1AA3",
                "Name" => "Kulim",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-1125-4FD0-908F-293F11051466",
                "Name" => "Langkawi",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-269D-4C23-9857-BFD22031CE03",
                "Name" => "Padang Terap",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-3517-4914-A673-3A3523CEF328",
                "Name" => "Pendang",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-4776-4826-B5F9-7C9AF94BA604",
                "Name" => "Pokok Sena",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-4928-4672-9058-77EA79C25805",
                "Name" => "Sik",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-4BCA-48FF-BB9A-06BA3DACB093",
                "Name" => "Yan",
                "NegeriId" => "994DADA4-1FC5-49A9-B25A-88DC5DD99150",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-4D95-4248-8500-3505FBBFC722",
                "Name" => "Bachok",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-4EC8-41F6-90BD-07E6EA944F4D",
                "Name" => "Kota Bharu",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5003-4002-AABE-0FC7699B63BE",
                "Name" => "Machang",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5181-4633-ABA8-91112D722EFD",
                "Name" => "Pasir Mas",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-524D-471A-972B-D56BF5D247EE",
                "Name" => "Pasir Puteh",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5361-411B-B8E4-A0D5F193080F",
                "Name" => "Tanah Merah",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-54FB-4F8B-8E58-FBBF310D26CF",
                "Name" => "Tumpat",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5734-4305-A35C-6ABDABFFFA9D",
                "Name" => "Gua Musang",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5888-43F9-9CCA-A283E3592ACD",
                "Name" => "Kuala Krai",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5A19-462E-80A4-FDFBF3066563",
                "Name" => "Jeli",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5B37-4E2D-90C1-F5A1F8BEC8D5",
                "Name" => "Ketereh",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5C29-4B09-966F-7C42C8AACAA4",
                "Name" => "Melor",
                "NegeriId" => "994DADA4-22D8-44B8-803C-7B41CC5AAAB9",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5DDE-4454-9736-A3F94B8766D3",
                "Name" => "Alor Gajah",
                "NegeriId" => "994DADA4-23BA-46BD-B561-8BC15F486091",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5EDF-433F-AACD-39734C3C0B82",
                "Name" => "Jasin",
                "NegeriId" => "994DADA4-23BA-46BD-B561-8BC15F486091",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-5FED-406A-8EA4-C13DBF846945",
                "Name" => "Melaka Tengah",
                "NegeriId" => "994DADA4-23BA-46BD-B561-8BC15F486091",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6156-41C8-82E5-F42E6077B822",
                "Name" => "Jelebu",
                "NegeriId" => "994DADA4-24BD-4083-81BC-52332806B322",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6235-4103-9B6F-3710114A1B15",
                "Name" => "Kuala Pilah",
                "NegeriId" => "994DADA4-24BD-4083-81BC-52332806B322",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-647F-42C2-A2D5-F96236C65C17",
                "Name" => "Port Dickson",
                "NegeriId" => "994DADA4-24BD-4083-81BC-52332806B322",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-668D-4846-8C52-F001C3736AB6",
                "Name" => "Rembau",
                "NegeriId" => "994DADA4-24BD-4083-81BC-52332806B322",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-675E-4B08-A913-5AAF0D9F5A90",
                "Name" => "Seremban",
                "NegeriId" => "994DADA4-24BD-4083-81BC-52332806B322",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6831-407E-B2F4-A1CFF40B02FF",
                "Name" => "Tampin",
                "NegeriId" => "994DADA4-24BD-4083-81BC-52332806B322",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6909-4E45-AA42-C16B7079440C",
                "Name" => "Jempol",
                "NegeriId" => "994DADA4-24BD-4083-81BC-52332806B322",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6A2F-4E5F-A8BD-5F56DF4C7CE3",
                "Name" => "Gemas",
                "NegeriId" => "994DADA4-24BD-4083-81BC-52332806B322",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6B02-45AC-AD79-E9B7E781BBBE",
                "Name" => "Bentong",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6CA5-4318-B727-B18EB51E6E00",
                "Name" => "Cameron Highlands",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6DD7-459A-892E-41443810B7A3",
                "Name" => "Jerantut",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6EE7-4B2F-A6BB-D0DC904195A9",
                "Name" => "Kuantan",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-6FF4-4273-B83F-6F04BF285CF0",
                "Name" => "Lipis",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-7198-4B65-9D10-812215707452",
                "Name" => "Pekan",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-7287-428A-B2A8-29535E515913",
                "Name" => "Raub",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-7359-4E8C-AF26-35A03A8EF2DD",
                "Name" => "Temerloh",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-74BB-4616-B707-72648A337ADD",
                "Name" => "Rompin",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-75A7-4DAF-9A30-5F5AB5E09CBC",
                "Name" => "Maran",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-7712-4483-8941-222B35B9981D",
                "Name" => "Bera",
                "NegeriId" => "994DADA4-25C6-4103-B6BB-768653CFA13E",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-781B-4E75-8E99-AAF0037FCB57",
                "Name" => "Batang Padang",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-7A4B-400E-BBBB-B5F3D1A93B48",
                "Name" => "Manjung",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-7B87-4B9C-A121-38DD70359CB3",
                "Name" => "Kinta",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-7C96-4624-BBC4-96B39B59C70C",
                "Name" => "Kerian",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-7DD4-42FC-8200-E87B98E44FE3",
                "Name" => "Kuala Kangsar",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-7F4C-4440-B82E-89B50D846FA5",
                "Name" => "Larut, Matang dan Selama",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-8106-4328-B7C3-7AD698A0ECC7",
                "Name" => "Hilir Perak",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-8203-423A-9183-6FA0F48EC9BB",
                "Name" => "Hulu Perak",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-8319-41FC-8BE6-64B1961D4FC0",
                "Name" => "Perak Tengah",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-84B2-4993-9365-F44F3FC0D899",
                "Name" => "Bagan Datuk",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-85BA-40CB-9000-B08822C8EECC",
                "Name" => "Kampar",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-86E7-47EE-8D00-8D4CEEFB540D",
                "Name" => "Muallim",
                "NegeriId" => "994DADA4-2657-40DE-B05C-4F016AAC3F3B",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-87F3-41BA-BE33-80A972DE82D0",
                "Name" => "Perlis",
                "NegeriId" => "994DADA4-2718-4246-8BD0-039B667D68D6",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-88DE-4941-96BC-75C45E2EFE8A",
                "Name" => "Seberang Prai Tengah",
                "NegeriId" => "994DADA4-27B5-41F3-8E4E-47898F66FB0D",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-89D9-49E2-94F5-CB70DC2E0775",
                "Name" => "Seberang Prai Utara",
                "NegeriId" => "994DADA4-27B5-41F3-8E4E-47898F66FB0D",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-8AC1-4AD1-8BE4-7481B15049AB",
                "Name" => "Seberang Prai Selatan",
                "NegeriId" => "994DADA4-27B5-41F3-8E4E-47898F66FB0D",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-90BE-4931-AA08-691F6DB4FA7A",
                "Name" => "Timur Laut",
                "NegeriId" => "994DADA4-27B5-41F3-8E4E-47898F66FB0D",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-9211-48A7-B42E-1E44F1C5B62F",
                "Name" => "Barat Daya",
                "NegeriId" => "994DADA4-27B5-41F3-8E4E-47898F66FB0D",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-932B-4C84-8DC0-2CA7D4B724F8",
                "Name" => "Beaufort",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-94E4-4EEB-9674-742B36D81951",
                "Name" => "Beluran",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-9659-457A-A92B-5E36AC1FDF2C",
                "Name" => "Keningau",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-9817-4E87-8078-A60B451F75A3",
                "Name" => "Kota Belud",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-9AB3-4197-8CCE-25840BD777A6",
                "Name" => "Kinabatangan",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-9D51-4BF1-955A-CAA8BC4BB1B1",
                "Name" => "Kota Kinabalu",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-9F97-49D6-A601-DA7E8BEB7A66",
                "Name" => "Kota Marudu",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-A08F-4026-AEA4-D5AE9391AEAF",
                "Name" => "Kuala Penyu",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-A383-4887-840F-970FBE16CCD5",
                "Name" => "Kudat",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-A62D-485F-9DB5-77895AF6437C",
                "Name" => "Kunak",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-A79A-4BEA-A9A2-401DDDFD447E",
                "Name" => "Lahad Datu",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-A8F7-49AD-B3EF-8B7331D2A213",
                "Name" => "Nabawan",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-AA6E-4B47-910C-CF16C5AC69E0",
                "Name" => "Papar",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-AC26-4520-B064-4A8D62AE2AE1",
                "Name" => "Penampang",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-AF1D-4F54-B468-ADDEF32E8C36",
                "Name" => "Putatan",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-B01A-454F-8656-1405756488A7",
                "Name" => "Pitas",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-B12B-4702-83E5-6F4BADB0A3E7",
                "Name" => "Ranau",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-B30F-4EBB-A1EF-D939F055EFEC",
                "Name" => "Sandakan",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-B5B1-463C-9D34-12FB5287B558",
                "Name" => "Semporna",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-B6A7-42C5-ACFB-D5C85AF1F4E3",
                "Name" => "Sipitang",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-B7D1-46C4-9CAA-BBC52C4EB5D8",
                "Name" => "Tambunan",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-B90C-4390-8F95-5858E2109E41",
                "Name" => "Tawau",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-BA06-47E8-8EF4-4C5502C64253",
                "Name" => "Telupid",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-BB0A-4213-BFDB-67EB3AAA2D8E",
                "Name" => "Tenom",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-BBFD-4749-A301-8FA2B5257362",
                "Name" => "Tongod",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-BCF8-4259-B638-9B443B38355E",
                "Name" => "Tuaran",
                "NegeriId" => "994DADA4-284B-43CF-AC37-F497D13E36B3",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-BF63-42CD-86BC-4100581A3D1A",
                "Name" => "Kuching",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-C04A-48C0-B7E5-B41F6037DC9F",
                "Name" => "Bau",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-C1C2-41F7-B7F4-BEEE4FFC1ED5",
                "Name" => "Lundu",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-C29E-4E16-873F-AA38E9C968DE",
                "Name" => "Samarahan",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-C374-4E9F-AFDE-B3B4A49CCE66",
                "Name" => "Serian",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-C4E3-436F-AD01-AF7CD1357D63",
                "Name" => "Simunjan",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-C627-46FD-9AD9-305D4EB273D8",
                "Name" => "Asajaya",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-C707-47FF-AE2E-B29113CADC55",
                "Name" => "Sri Aman",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-C8BD-4EB0-A5F6-6380B3EAF735",
                "Name" => "Lubok Antu",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-C9DA-4090-9948-A0D92D0E4AE8",
                "Name" => "Betong",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-CB1B-4EF8-9999-CF315BCA2A1B",
                "Name" => "Saratok",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-CC79-4784-A9CB-E0B791B975B8",
                "Name" => "Sarikei",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-CDD6-403E-8B02-8653AFFB0C94",
                "Name" => "Maradong",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-CF13-4814-ACC1-F58D02221A07",
                "Name" => "Julau",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-D061-4BEB-8797-D14D2C9A506E",
                "Name" => "Pakan",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-D1E2-4865-A7F0-826FADCC3E0C",
                "Name" => "Sibu",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-D324-4D58-ACF3-85BD41F45C0E",
                "Name" => "Kanowit",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-D45E-451A-A3FD-E2A383834BD2",
                "Name" => "Selangau",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-D666-48C0-8498-4824EF260416",
                "Name" => "Mukah",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-D776-4EEC-BDB0-C6C87E4B79BD",
                "Name" => "Dalat",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-D9CB-4DAB-9589-FCE72B20D518",
                "Name" => "Matu",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-DAD1-427E-B018-B753C90E3672",
                "Name" => "Daro",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-DBD1-466F-B328-7DE1C3C1C867",
                "Name" => "Bintulu",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-DCE9-4838-88B5-29236402DF5C",
                "Name" => "Tatau",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-DDE2-443D-88D0-9CB93021EF40",
                "Name" => "Kapit",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-DEFB-4F2B-8645-369468200588",
                "Name" => "Song",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-E188-49DB-8FD1-B2A9CD2E5296",
                "Name" => "Belaga",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-E7E2-43DA-B054-19B5886B34EC",
                "Name" => "Miri",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-E9F7-4574-99F0-A14E88B299D2",
                "Name" => "Marudi",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-EB38-44C0-BC05-B1CC3F92D0F6",
                "Name" => "Limbang",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-EE50-47CE-A6D1-352F8D4B1B25",
                "Name" => "Lawas",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-EFA2-4E62-9CD5-4264F8E5F660",
                "Name" => "Gedung",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-F0AD-4EB6-95E7-DE3DAD463093",
                "Name" => "Kabong",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-F1A7-4352-B1ED-FE7195C2B314",
                "Name" => "Pusa",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-F3C6-4602-9028-E474DC8FE867",
                "Name" => "Bukit Mabong",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-F4CD-4DEA-9246-24AE0CEBD847",
                "Name" => "Beluru",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-F5BB-4972-B8B6-8B9C91658137",
                "Name" => "Subis",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-F6CB-41CE-91E7-B5F5E1032EFC",
                "Name" => "Telang Usan",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-F7CB-4297-BB92-D720301D10D4",
                "Name" => "Tanjung Manis",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-F91C-405E-888F-E4C8BDF02466",
                "Name" => "Meradong",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-FA1B-424D-A5E6-533E1C92F1AE",
                "Name" => "Tebedu",
                "NegeriId" => "994DADA4-2926-485C-A70C-B58E735FE390",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-FB41-4EB8-A12D-4E696037F839",
                "Name" => "Gombak",
                "NegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-FC56-49C7-9874-6E3B258391E6",
                "Name" => "Klang",
                "NegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-FD66-4869-90B8-26964CCD4476",
                "Name" => "Kuala Langat",
                "NegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-FE57-4115-8BEA-40E31E82D329",
                "Name" => "Kuala Selangor",
                "NegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB531-FF80-4067-8277-8C3365FA33E8",
                "Name" => "Petaling",
                "NegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0064-4F1A-939B-48987472B4E2",
                "Name" => "Sabak Bernam",
                "NegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-01EF-4C0D-9937-EAE5DBFBB1B7",
                "Name" => "Sepang",
                "NegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-037D-4C6E-AE51-CFC73663C74F",
                "Name" => "Hulu Langat",
                "NegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0469-456E-8EC1-ED6E14991397",
                "Name" => "Hulu Selangor",
                "NegeriId" => "994DADA4-29C3-4742-8AB9-66C1C3E09859",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0594-4BFB-9B6E-4ED1B67EC1AE",
                "Name" => "Besut",
                "NegeriId" => "994DADA4-2B41-4863-9509-7320773682FF",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-073D-45B5-9877-99C3B5EB13CD",
                "Name" => "Dungun",
                "NegeriId" => "994DADA4-2B41-4863-9509-7320773682FF",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-085C-4586-8141-A55BF1FF9ABB",
                "Name" => "Kemaman",
                "NegeriId" => "994DADA4-2B41-4863-9509-7320773682FF",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0943-4C87-B564-BE6801408734",
                "Name" => "Kuala Terengganu",
                "NegeriId" => "994DADA4-2B41-4863-9509-7320773682FF",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0A5C-41CE-9975-4BA3DB5D9A86",
                "Name" => "Marang",
                "NegeriId" => "994DADA4-2B41-4863-9509-7320773682FF",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0B46-4ADF-9DB3-0FA0043F9312",
                "Name" => "Hulu Terengganu",
                "NegeriId" => "994DADA4-2B41-4863-9509-7320773682FF",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0CAC-4FBD-B161-70BE26D21991",
                "Name" => "Setiu",
                "NegeriId" => "994DADA4-2B41-4863-9509-7320773682FF",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0DFC-4FA5-89E0-4ADD4B33B9DC",
                "Name" => "Kuala Nerus",
                "NegeriId" => "994DADA4-2B41-4863-9509-7320773682FF",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0ED9-4C33-89FD-F6FE9341F0D9",
                "Name" => "Kuala Lumpur",
                "NegeriId" => "994DADA4-2BED-4F81-B3CB-E877C5EDD2C5",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-0FCF-479D-8009-53F1B80709B9",
                "Name" => "Labuan",
                "NegeriId" => "994DADA4-2BED-4F81-B3CB-E877C5EDD2C5",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            ),
            array(
                "DaerahId" => "994DB532-10E6-434B-9787-F3AFA970D631",
                "Name" => "Putrajaya",
                "NegeriId" => "994DADA4-2BED-4F81-B3CB-E877C5EDD2C5",
                "created_at" => "2023-06-01T03:31:54.000Z",
                "updated_at" => "2023-06-01T03:31:54.000Z"
            )
        );

        foreach ($Daerah as $daerah) {
            ModelsRefDaerah::create([
                "RefDaerahId" => $daerah["DaerahId"],
                "Daerah" => $daerah["Name"],
                "RefNegeriId" => $daerah["NegeriId"],
                "created_at" => now(),
                "updated_at" => now()
            ]);
        }
    }
}
