<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermohonanAsnaf\PermohonanAsnafRequest;
use App\Models\PermohonanAkaun;
use App\Models\PermohonanAsnaf;
use App\Models\RefNegeri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermohonanAkaunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.permohonan-akaun.index', [
            'negeris' => RefNegeri::all(),
            'page' => 'permohonan'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermohonanAsnafRequest $request)
    {
        DB::beginTransaction();

        try {
            PermohonanAsnaf::create($request->all());
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }

        return redirect()->route('permohonan-akaun.index')->with(
            [
                'type' => 'success',
                'icon' => 'check',
                'message' => "Permohonan anda telah dihantar, Permohonan akan disemak dahulu."
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PermohonanAkaun  $permohonanAkaun
     * @return \Illuminate\Http\Response
     */
    public function show(PermohonanAkaun $permohonanAkaun)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PermohonanAkaun  $permohonanAkaun
     * @return \Illuminate\Http\Response
     */
    public function edit(PermohonanAkaun $permohonanAkaun)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PermohonanAkaun  $permohonanAkaun
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PermohonanAkaun $permohonanAkaun)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PermohonanAkaun  $permohonanAkaun
     * @return \Illuminate\Http\Response
     */
    public function destroy(PermohonanAkaun $permohonanAkaun)
    {
        //
    }
}
