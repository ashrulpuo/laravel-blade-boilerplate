<?php

namespace App\Http\Controllers\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Requests\Tetapan\RoleRequests;
use App\Models\Module;
use App\Models\RoleModule;
use Illuminate\Pagination\Paginator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    protected $fieldSearchable = [

		'Role',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $input = $request->all();

            Paginator::currentPageResolver(function () use ($input) {
                return ($input['start'] / $input['length'] + 1);
            });

            $model = new Role();

            if (!empty($input['search']['value'])) {
                foreach ($this->fieldSearchable as $column) {
                    $model = $model->whereLike($column, $input['search']['value']);
                }
            }

            $model = $model->paginate($input['length']);
            $output = $model->toArray();

            $response = [
                "draw"            => $input['draw'],
                "recordsTotal"    => intval($output['total']),
                "recordsFiltered" => intval($output['total']),
                "data"            => $output['data']
            ];

            return response()->json($response, 200);
        }

        return view('tetapan.role.index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequests $request)
    {
        $input = $request->all();

        DB::beginTransaction();
        try {
            $role = Role::create([
                'Role' => $input['Role']
            ]);

            foreach ($input['module'] as $key => $value) {
                RoleModule::create([
                    'RoleId' => $role->RoleId,
                    'ModulesId' => $value
                ]);
            }

            DB::commit();
            return redirect()->route('Role.index')->with([
                'type' => 'success',
                'code' => 200,
                'message' => 'Tetapan Berjaya Disimpan'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $Role = Role::with('Module')->where('RoleId', $id)->first();

            $modules = Module::all();

            $selectedId = [];
            foreach ($Role->Module as $key => $value) {
                $selectedId[] = $value->ModulesId;
            }

            return view('tetapan.role.edit', [
                'role' => $Role,
                'modules' => $modules,
                'selectedId' => $selectedId
            ]);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequests $request, $id)
    {
        $input = $request->all();

        try {
            $Role = Role::where('RoleId',$id)->get()->first();
            $Role->update($input);

            RoleModule::where('RoleId', $id)->delete();

            foreach ($input['module'] as $key => $value) {
                RoleModule::create([
                    'RoleId' => $id,
                    'ModulesId' => $value
                ]);
            }

            return redirect()->route('Role.index')->with([
                'type' => 'success',
                'code' => 200,
                'message' => 'Kemaskini tetapan berjaya'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Role::destroy($id);
            return response()->json([
                'type' => 'success',
                'code' => 200,
                'message' => 'Tetapan berjaya dipadam'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function createRole()
    {
        $modules = Module::all();

        return view('tetapan.role.create')->with([
            'modules' => $modules
        ]);
    }
}
