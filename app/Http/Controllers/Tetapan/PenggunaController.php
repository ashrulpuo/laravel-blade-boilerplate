<?php

namespace App\Http\Controllers\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use App\Http\Requests\Tetapan\PenggunaRequests;
use App\Http\Requests\Tetapan\ProfileRequest;
use App\Models\MaklumatPengguna;
use App\Models\Refbahagian;
use App\Models\RefJawatan;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Pagination\Paginator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{
    protected $fieldSearchable = [

		'name',
		'email',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $input = $request->all();

            Paginator::currentPageResolver(function () use ($input) {
                return ($input['start'] / $input['length'] + 1);
            });

            $model = User::with(['MaklumatPengguna', 'MaklumatPengguna.bahagian', 'role', 'role.role']);

            if (!empty($input['search']['value'])) {
                foreach ($this->fieldSearchable as $column) {
                    $model = $model->whereLike($column, $input['search']['value']);
                }
            }

            $model = $model->paginate($input['length']);
            $output = $model->toArray();

            $response = [
                "draw"            => $input['draw'],
                "recordsTotal"    => intval($output['total']),
                "recordsFiltered" => intval($output['total']),
                "data"            => $output['data']
            ];

            return response()->json($response, 200);
        }

        return view('tetapan.pengguna.index')->with([
            'RefBahagian' => Refbahagian::all(),
            'RefJawatan' => RefJawatan::all(),
            'Role' => Role::all()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PenggunaRequests $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make('P@ssw0rd');

        if(in_array('Penyiasat', $input)) {
            $input['Penyiasat'] = 10;
        }

        DB::beginTransaction();
        try {
            $pengguna = Pengguna::create($input);

            MaklumatPengguna::create([
                'PenggunaId' => $pengguna->PenggunaId,
                'BahagianId'=> $input['RefBahagianId'],
                'JawatanId'=> $input['JawatanId'],
                'NoTel' => $input['NoTel'],
                'NoTelPejabat' => $input['NoTelPejabat'],
                'Penyiasat' => $input['Penyiasat'] ?? 0,
                'Pentadbir' => !empty($input['admin']) ? 1 : 0,
                'created_at' => Carbon::now()->format('Y-m-d'),
                'updated_at' => Carbon::now()->format('Y-m-d'),
            ]);

            foreach ($input['RoleId'] as $key => $value) {
                UserRole::create([
                    'PenggunaId' => $pengguna->PenggunaId,
                    'RoleId' => $value
                ]);
            }

            DB::commit();

            return redirect()->route('Pengguna.index')->with([
                    'type' => 'success',
                    'code' => 200,
                    'message' => 'Tetapan Berjaya Disimpan'
                ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {
            $Pengguna = User::with(['MaklumatPengguna', 'role'])->where('PenggunaId', $id)->get()->first();

            $roleId = [];
            if($Pengguna->role) {
                foreach ($Pengguna->role as $key => $value) {
                    $roleId[] = $value->RoleId;
                }
            }

            return view('tetapan.pengguna.kemaskini')->with([
                'RefBahagian' => Refbahagian::all(),
                'RefJawatan' => RefJawatan::all(),
                'Role' => Role::all(),
                'Pengguna' => $Pengguna,
                'selectedRole' => $roleId
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PenggunaRequests $request, $id)
    {
        $input = $request->all();

        try {
            $Pengguna = User::where('PenggunaId',$id)->get()->first();
            $Pengguna->update($input);

            $Pengguna->MaklumatPengguna->update([
                'NoTel' => $input['NoTel'],
                'NoTelPejabat' => $input['NoTelPejabat'],
                'BahagianId'=> $input['RefBahagianId'],
                'JawatanId'=> $input['JawatanId'],
                'Penyiasat' => $input['Penyiasat'] ?? 0,
                'RoleId' => $input['RoleId'],
                'Pentadbir' => !empty($input['admin']) ? 1 : 0,
                'updated_at' => Carbon::now(),
            ]);

            UserRole::where('PenggunaId', $id)->delete();

            foreach ($input['RoleId'] as $key => $value) {
                UserRole::create([
                    'PenggunaId' => $id,
                    'RoleId' => $value
                ]);
            }

            return redirect()->route('Pengguna.index')->with([
                'type' => 'success',
                'code' => 200,
                'message' => 'Kemaskini tetapan berjaya'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Pengguna::destroy($id);
            return response()->json([
                'type' => 'success',
                'code' => 200,
                'message' => 'Tetapan berjaya dipadam'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function create()
    {
        return view('tetapan.pengguna.create')->with([
            'RefBahagian' => Refbahagian::all(),
            'RefJawatan' => RefJawatan::all(),
            'Role' => Role::all()
        ]);
    }

    public function profile($id)
    {
        try {
            $Pengguna = User::with(['MaklumatPengguna', 'role'])->where('PenggunaId', $id)->get()->first();

            $roleId = [];
            if($Pengguna->role) {
                foreach ($Pengguna->role as $key => $value) {
                    $roleId[] = $value->RoleId;
                }
            }

            return view('tetapan.pengguna.profile')->with([
                'RefBahagian' => Refbahagian::all(),
                'RefJawatan' => RefJawatan::all(),
                'Role' => Role::all(),
                'Pengguna' => $Pengguna,
                'selectedRole' => $roleId
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function updateProfile(ProfileRequest $request, $id)
    {
        $input = $request->all();

        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        } else {
            unset($input['password']);
        }

        try {
            $Pengguna = User::where('PenggunaId',$id)->get()->first();
            $Pengguna->update($input);

            $Pengguna->MaklumatPengguna->update([
                'NoTel' => $input['NoTel'],
                'NoTelPejabat' => $input['NoTelPejabat'],
                'BahagianId'=> $input['RefBahagianId'],
                'JawatanId'=> $input['JawatanId'],
                'Penyiasat' => $input['Penyiasat'] ?? 0,
                'Pentadbir' => !empty($input['admin']) ? 1 : 0,
                'updated_at' => Carbon::now(),
            ]);

            return redirect()->route('profil', $id)->with([
                'type' => 'success',
                'code' => 200,
                'message' => 'Kemaskini profile pengguna berjaya'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function resetPassword($id)
    {
        $Pengguna = User::where('PenggunaId',$id)->get()->first();
        $Pengguna->update([
            'password' => Hash::make('P@ssw0rd')
        ]);

        return redirect()->route('Pengguna.index', $id)->with([
            'type' => 'success',
            'code' => 200,
            'message' => 'Reset kata laluan pengguna berjaya'
        ]);
    }

    /*
    public function filter($val)
    {
        try {
            $.'Pengguna' = 'PenggunaId'::orderBy('DaftarPada', 'asc')
                ->where('KodAgama', 'LIKE', '%' . $val . '%')
                ->orWhere('Penerangan', 'LIKE', '%' . $val . '%')
                ->get();

          return $agama;
        } catch (\Throwable $th) {
            throw $th;
           return ['agama' => 'ralat di filter function'];
       }
    }
    */
}


