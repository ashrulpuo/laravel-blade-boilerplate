<?php

namespace App\Http\Controllers\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\Routes;
use Illuminate\Http\Request;
use App\Http\Requests\Tetapan\RoutesRequests;
use Illuminate\Pagination\Paginator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class RoutesController extends Controller
{
    protected $fieldSearchable = [

		'Method',
		'Uri',
		'Name',
		'Controller',
		'Function',
		'Namespace',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $input = $request->all();

            Paginator::currentPageResolver(function () use ($input) {
                return ($input['start'] / $input['length'] + 1);
            });

            $model = new Routes();

            if (!empty($input['search']['value'])) {
                foreach ($this->fieldSearchable as $column) {
                    $model = $model->whereLike($column, $input['search']['value']);
                }
            }

            $model = $model->paginate($input['length']);
            $output = $model->toArray();

            $response = [
                "draw"            => $input['draw'],
                "recordsTotal"    => intval($output['total']),
                "recordsFiltered" => intval($output['total']),
                "data"            => $output['data']
            ];

            return response()->json($response, 200);
        }

        return view('tetapan.routes.index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $routeCollection    = Route::getRoutes()->get();
        $routes             = Routes::all('Hash')->toArray();
        $hash               = [];
        $route_hash         = [];
        $ar                 = [];


        if ($routes) {

            array_walk($routes, function ($value, $key) use (&$hash) {

                $hash[] = array_values($value)[0];
            });
        }

        $new_route = 0;

        DB::beginTransaction();
        try {
            foreach (collect($routeCollection)->chunk(100) as $row) {
                $x = 0;
                $ar = [];
                foreach ($row as $d) {
                   // dd( $d->action['uses'] );
                    $controller = isset($d->action['controller']) ? $d->action['controller'] : 'Closure';
                    $name       = isset($d->action['as']) ? $d->action['as'] : '-';
                    $function   = '-';

                    if (isset($d->action['controller'])) {

                        $arr_cont = explode('\\', $d->action['controller']);

                        if (count($arr_cont) > 1) {

                            $method_name    = explode('@', $arr_cont[count($arr_cont) - 1]);

                            $controller     = isset($method_name[0]) ? $method_name[0] : '-';

                            $function       = isset($method_name[1]) ? $method_name[1] : '-';
                        }
                    }

                    $namespace  = isset( $d->action['namespace'] ) ? $d->action['namespace'] : '-';
                    $method     = implode('.', $d->methods);

                    $route_hash[]            = md5($method . $d->uri . $controller . $function . $namespace);

                    if (count($hash) >  0) {

                        if (in_array(md5($method . $d->uri . $controller . $function. $namespace), $hash)) {
                            continue;
                        }

                        $new_route++;
                        // echo $controller.' -- '.boolval( in_array( md5($method.$d->uri.$controller.$function), $hash ) ).'<br>';
                    }

                    $ar[$x]['RoutesId']      = Str::uuid()->toString();
                    $ar[$x]['Method']        = $method;
                    $ar[$x]['Uri']           = $d->uri;
                    $ar[$x]['Namespace']     = $namespace;
                    $ar[$x]['Controller']    = $controller;
                    $ar[$x]['Name']          = $name;
                    $ar[$x]['Function']      = $function;
                    $ar[$x]['Hash']          = md5($method . $d->uri . $controller . $function. $namespace );

                    $x++;

                }
                if (count($ar) > 0) Routes::insert($ar);
                DB::commit();
            }

            return response()->json([
                'type' => 'success',
                'code' => 200,
                'message' => 'Tetapan Berjaya Disimpan'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $Routes = Routes::where('RoutesId', $id)->get()->first();
            return response()->json([
                'type' => 'success',
                'code' => 200,
                'data' => $Routes
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoutesRequests $request, $id)
    {
        $input = $request->all();

        try {
            $Routes = Routes::where('RoutesId',$id)->get()->first();
            $Routes->update($input);
            return response()->json([
                'type' => 'success',
                'code' => 200,
                'message' => 'Kemaskini tetapan berjaya'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Routes::destroy($id);
            return response()->json([
                'type' => 'success',
                'code' => 200,
                'message' => 'Tetapan berjaya dipadam'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /*
    public function filter($val)
    {
        try {
            $.'Routes' = 'RoutesId'::orderBy('DaftarPada', 'asc')
                ->where('KodAgama', 'LIKE', '%' . $val . '%')
                ->orWhere('Penerangan', 'LIKE', '%' . $val . '%')
                ->get();

          return $agama;
        } catch (\Throwable $th) {
            throw $th;
           return ['agama' => 'ralat di filter function'];
       }
    }
    */
}
