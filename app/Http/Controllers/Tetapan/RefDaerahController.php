<?php

namespace App\Http\Controllers\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\RefDaerah;
use Illuminate\Http\Request;

class RefDaerahController extends Controller
{
    public function getDaerah($negeriId)
    {
        $daerahs = RefDaerah::where('RefNegeriId', $negeriId)->get();
        return response()->json([
            'daerahs' => $daerahs,
        ]);
    }
}
