<?php

namespace App\Http\Controllers\Tetapan;

use App\Http\Controllers\Controller;
use App\Models\Module;
use Illuminate\Http\Request;
use App\Http\Requests\Tetapan\ModuleRequests;
use App\Models\ModuleRoute;
use App\Models\Routes;
use Illuminate\Pagination\Paginator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ModuleController extends Controller
{
    protected $fieldSearchable = [

		// 'Name',
        // 'Controller',
        'Function'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $input = $request->all();

            Paginator::currentPageResolver(function () use ($input) {
                return ($input['start'] / $input['length'] + 1);
            });

            $model = Module::withCount('routes');

            if (!empty($input['search']['value'])) {
                foreach ($this->fieldSearchable as $column) {
                    $model = $model->whereLike($column, $input['search']['value']);
                }
            }

            $model = $model->paginate($input['length']);
            $output = $model->toArray();

            $response = [
                "draw"            => $input['draw'],
                "recordsTotal"    => intval($output['total']),
                "recordsFiltered" => intval($output['total']),
                "data"            => $output['data']
            ];

            return response()->json($response, 200);
        }

        return view('tetapan.module.index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModuleRequests $request)
    {
        $input = $request->all();

        DB::beginTransaction();
        try {
            Module::create($input);
            DB::commit();
            return response()->json([
                'type' => 'success',
                'code' => 200,
                'message' => 'Tetapan Berjaya Disimpan'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $Module = Module::where('ModuleId', $id)->get()->first();
            return response()->json([
                'type' => 'success',
                'code' => 200,
                'data' => $Module
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ModuleRequests $request, $id)
    {
        $input = $request->all();

        try {
            $Module = Module::where('ModuleId',$id)->get()->first();
            $Module->update($input);
            return response()->json([
                'type' => 'success',
                'code' => 200,
                'message' => 'Kemaskini tetapan berjaya'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Module::destroy($id);
            return response()->json([
                'type' => 'success',
                'code' => 200,
                'message' => 'Tetapan berjaya dipadam'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /*
    public function filter($val)
    {
        try {
            $.'Module' = 'ModuleId'::orderBy('DaftarPada', 'asc')
                ->where('KodAgama', 'LIKE', '%' . $val . '%')
                ->orWhere('Penerangan', 'LIKE', '%' . $val . '%')
                ->get();

          return $agama;
        } catch (\Throwable $th) {
            throw $th;
           return ['agama' => 'ralat di filter function'];
       }
    }
    */


    public function RouteDt(Request $request)
    {
        if($request->ajax()){
            $input = $request->all();

            Paginator::currentPageResolver(function () use ($input) {
                return ($input['start'] / $input['length'] + 1);
            });

            $model = new Routes();

            if (!empty($input['search']['value'])) {
                foreach ($this->fieldSearchable as $column) {
                    $model = $model->whereLike($column, $input['search']['value']);
                }
            }

            $model = $model->paginate($input['length']);
            $output = $model->toArray();

            $response = [
                "draw"            => $input['draw'],
                "recordsTotal"    => intval($output['total']),
                "recordsFiltered" => intval($output['total']),
                "data"            => $output['data']
            ];

            return response()->json($response, 200);
        }
    }

    public function routeList(Request $request, $moduleId)
    {
        if($request->ajax()){
            $input = $request->all();

            Paginator::currentPageResolver(function () use ($input) {
                return ($input['start'] / $input['length'] + 1);
            });

            $model = Routes::whereNotIn('RoutesId',function($query) use ($moduleId) {
                    $query->select('RouteId')->from('ModuleRoute')->where('ModuleId', $moduleId);
                });

            if (!empty($input['search']['value'])) {
                // $model = $model->where('Function', 'LIKE', '%' . $input['search']['value'] . '%');
                $model = $model->where(function($query) use ($input){
                    $query->where('Name', 'LIKE', '%' . $input['search']['value'] . '%')
                            ->orWhere('Controller', 'LIKE', '%' . $input['search']['value'] . '%')
                            ->orWhere('Function', 'LIKE', '%' . $input['search']['value'] . '%');
                });

                // foreach ($this->fieldSearchable as $column) {
                //     $model = $model->whereLike($column, $input['search']['value']);
                // }
            }

            $model = $model->paginate($input['length']);
            $output = $model->toArray();

            $response = [
                "draw"            => $input['draw'],
                "recordsTotal"    => intval($output['total']),
                "recordsFiltered" => intval($output['total']),
                "data"            => $output['data']
            ];

            return response()->json($response, 200);
        }
    }

    public function storeModuleRoute(Request $request)
    {
        $input = $request->all();

        ModuleRoute::create([
            'ModuleId' => $input['ModuleId'],
            'RouteId' => $input['RouteId']
        ]);

        return response()->json(true);
    }

    public function hapusRouteList(Request $request, $moduleId)
    {
        if($request->ajax()){
            $input = $request->all();

            Paginator::currentPageResolver(function () use ($input) {
                return ($input['start'] / $input['length'] + 1);
            });

            $model = Routes::whereIn('RoutesId',function($query) use ($moduleId) {
                    $query->select('RouteId')->from('ModuleRoute')->where('ModuleId', $moduleId);
                });

            if (!empty($input['search']['value'])) {
                // $model = $model->where('Function', 'LIKE', '%' . $input['search']['value'] . '%');
                $model = $model->where(function($query) use ($input){
                    $query->where('Name', 'LIKE', '%' . $input['search']['value'] . '%')
                            ->orWhere('Controller', 'LIKE', '%' . $input['search']['value'] . '%')
                            ->orWhere('Function', 'LIKE', '%' . $input['search']['value'] . '%');
                });

                // foreach ($this->fieldSearchable as $column) {
                //     $model = $model->whereLike($column, $input['search']['value']);
                // }
            }

            $model = $model->paginate($input['length']);
            $output = $model->toArray();

            $response = [
                "draw"            => $input['draw'],
                "recordsTotal"    => intval($output['total']),
                "recordsFiltered" => intval($output['total']),
                "data"            => $output['data']
            ];

            return response()->json($response, 200);
        }
    }

    public function hapusModuleRoute(Request $request)
    {
        $input = $request->all();

        ModuleRoute::where('ModuleId', $input['ModuleId'])->where('RouteId', $input['RouteId'])->delete();

        return response()->json(true);
    }
}
