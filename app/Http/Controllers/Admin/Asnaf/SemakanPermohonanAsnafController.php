<?php

namespace App\Http\Controllers\Admin\Asnaf;

use App\Http\Controllers\Controller;
use App\Http\Services\PermohonanAsnaf\PermohonanAsnafService;
use App\Models\PermohonanAsnaf;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class SemakanPermohonanAsnafController extends Controller
{
    protected $PermohonanService;

    public function __construct()
    {
        // $this->PermohonanService = $PermohonanService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $input = $request->all();

            Paginator::currentPageResolver(function () use ($input) {
                return ($input['start'] / $input['length'] + 1);
            });

            $model = PermohonanAsnaf::orderBy('created_at', 'DESC');

            if (!empty($input['search']['value'])) {
                $model = $model->where('NoRujukan', 'LIKE', '%' . $input['search']['value'] . '%')->orWhere('TajukSumbangan', 'LIKE', '%' . $input['search']['value'] . '%');
            }

            $model = $model->paginate($input['length']);
            $output = $model->toArray();

            $response = [
                "draw"            => $input['draw'],
                "recordsTotal"    => intval($output['total']),
                "recordsFiltered" => intval($output['total']),
                "data"            => $output['data']
            ];

            return response()->json($response, 200);
        }


        return view('modules.admin.permohonan-asnaf.semakan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PermohonanAsnaf  $permohonanAsnaf
     * @return \Illuminate\Http\Response
     */
    public function show(PermohonanAsnaf $permohonanAsnaf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PermohonanAsnaf  $permohonanAsnaf
     * @return \Illuminate\Http\Response
     */
    public function edit($permohonanAsnaf)
    {
        $permohonan = PermohonanAsnaf::find($permohonanAsnaf);
        return view('modules.admin.permohonan-asnaf.semakan.edit', ['permohonan' => $permohonan]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PermohonanAsnaf  $permohonanAsnaf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $permohonanAsnafId)
    {
        DB::beginTransaction();
        try {
            $permohonan = PermohonanAsnaf::find($permohonanAsnafId);
            $permohonan->update([
                'Status' => $request->input('Tindakan'),
                'Catatan' => $request->input('Catatan'),
            ]);

            DB::commit();

            return redirect()->route('semakan-permohonan-asnaf.index')->with([
                'type' => 'success',
                'code' => 200,
                'message' => 'Kemaskini permohonan berjaya'
            ]);

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PermohonanAsnaf  $permohonanAsnaf
     * @return \Illuminate\Http\Response
     */
    public function destroy(PermohonanAsnaf $permohonanAsnaf)
    {
        DB::beginTransaction();

        try {
            $permohonanAsnaf->delete();
            DB::commit();

            return response()->json([
                'type' => 'success',
                'code' => 200,
                'message' => 'Permohonan Asnaf berjaya dihapus'
            ]);

        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}
