<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\UserRole;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    public function redirectTo() {
        $role = Auth::user()->role;

        foreach ($role as $key => $value) {
            if($value->RoleId == Role::PENGADU) {
                return '/pendaftaran';
            }
        }

        return '/home';
      }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        $routes = $this->getRoute($user);
        // dd($routes, $user);
        $request->session()->put('allowUrl', $routes);
    }

    public function getRoute($user)
    {
        $routes = UserRole::join('Role', 'UserRole.RoleId', '=', 'Role.RoleId')
            ->join('RoleModule', 'UserRole.RoleId', '=', 'RoleModule.RoleId')
            ->join('Module', 'RoleModule.ModulesId', '=', 'Module.ModuleId')
            ->join('ModuleRoute', 'ModuleRoute.ModuleId', '=', 'Module.ModuleId')
            ->join('Routes', 'Routes.RoutesId', '=', 'ModuleRoute.RouteId')
            ->where('UserRole.PenggunaId', $user->PenggunaId)
            ->select('Module.Keterangan', 'Routes.Uri')
            ->get();

        $cleanRoutes = [];
        foreach ($routes as $key => $value) {
            $cleanRoutes[] = $value->Uri;
        }

        return $cleanRoutes;
    }
}
