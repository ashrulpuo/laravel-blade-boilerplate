<?php

namespace App\Http\Services\PermohonanAsnaf\PermohonanAsnafService;

use App\Models\PermohonanAsnaf;

class PermohonanAsnafService
{
    protected $model;

    public function __construct()
    {
        $this->model = new PermohonanAsnaf();
    }

    public function permohonanById($id)
    {
        return $this->model->find($id);
    }
}
