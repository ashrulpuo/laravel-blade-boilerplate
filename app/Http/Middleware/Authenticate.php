<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Route;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {

        if (! $request->expectsJson()) {
            return route('login');
        }
    }

    protected function authenticate($request, array $guards)
    {
        $route = Route::current();

        if ($this->auth->check()) {

            $exclude = [
                'tetapan/profil/{id}',
                'tetapan/profil/updateProfile/{id}'
            ];

            // if (!in_array($route->uri, $request->session()->get('allowUrl')) && !in_array($route->uri, $exclude)) {
            //     abort(401);
            // }

        } else {

            $this->unauthenticated($request, $guards);
        }
    }
}
