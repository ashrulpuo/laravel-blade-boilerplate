<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PendaftaranAduanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'tajukAduan'    =>  'required',
            'jenisAduan'    =>  'required',
            'Alamat'        =>  'required',
            'data'          =>  'required',
            'catatanAduan'  =>  '',
        ];
    }
}
