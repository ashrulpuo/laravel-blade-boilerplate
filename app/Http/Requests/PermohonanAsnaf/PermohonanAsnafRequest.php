<?php

namespace App\Http\Requests\PermohonanAsnaf;

use Illuminate\Foundation\Http\FormRequest;

class PermohonanAsnafRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'Nama' => 'required|string|max:255',
            'NoKp' => 'required|string|size:12|unique:Pengguna,NoKp',
            'Email' => 'required|email|unique:Pengguna,email',
            'NoTel' => 'required|string|max:12',
            'Jantina' => 'required',
            'StatusPerkahwinan' => 'required',
            'Alamat' => 'required|string|max:255',
            'NegeriId' => 'required|exists:RefNegeri,RefNegeriId',
            'DaerahId' => 'required|exists:RefDaerah,RefDaerahId',
            'Poskod' => 'required|string|max:5',
            'Kariah' => 'required|string|max:255',
        ];
    }

    public function messages()
    {
        return [
            'Nama.required' => 'Sila isi nama.',
            'Nama.string' => 'Nama mesti dalam bentuk teks.',
            'Nama.max' => 'Nama mesti kurang daripada 255 aksara.',

            'NoKp.required' => 'Sila isi nombor kad pengenalan.',
            'NoKp.string' => 'Nombor kad pengenalan mesti dalam bentuk teks.',
            'NoKp.size' => 'Nombor kad pengenalan mesti mengandungi 12 digit.',
            'NoKp.unique' => 'Nombor kad pengenalan ini sudah wujud dalam pangkalan data.',

            'Email.required' => 'Sila isi alamat emel.',
            'Email.email' => 'Sila masukkan alamat emel yang sah.',
            'Email.unique' => 'Alamat emel ini sudah wujud dalam pangkalan data.',

            'NoTel.required' => 'Sila isi nombor telefon.',
            'NoTel.string' => 'Nombor telefon mesti dalam bentuk teks.',
            'NoTel.max' => 'Nombor telefon mesti kurang daripada 12 aksara.',

            'Jantina.required' => 'Sila pilih jantina.',

            'StatusPerkahwinan.required' => 'Sila pilih status perkahwinan.',

            'Alamat.required' => 'Sila isi alamat.',
            'Alamat.string' => 'Alamat mesti dalam bentuk teks.',
            'Alamat.max' => 'Alamat mesti kurang daripada 255 aksara.',

            'NegeriId.required' => 'Sila pilih negeri.',
            'NegeriId.exists' => 'Negeri yang dipilih tidak sah.',

            'DaerahId.required' => 'Sila pilih daerah.',
            'DaerahId.exists' => 'Daerah yang dipilih tidak sah.',

            'Poskod.required' => 'Sila isi poskod.',
            'Poskod.string' => 'Poskod mesti dalam bentuk teks.',
            'Poskod.max' => 'Poskod mesti kurang daripada 5 aksara.',

            'Kariah.required' => 'Sila isi kariah.',
            'Kariah.string' => 'Kariah mesti dalam bentuk teks.',
            'Kariah.max' => 'Kariah mesti kurang daripada 255 aksara.',
        ];

    }
}
