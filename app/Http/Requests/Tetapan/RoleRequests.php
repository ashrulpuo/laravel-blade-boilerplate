<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RoleRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'RoleId' => '',
			'Role' => !empty($this->request->get('RoleId')) ? ['required',Rule::unique('Role')->ignore($this->Role, 'Role')] : 'required|unique:Role,Role',
			'created_at' => '',
            'updated_at' => '',
            "module"  => "required|array",
        ];
    }
}
