<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class BajetRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'BajetId' => '',
			'NamaBajet' => !empty($this->BajetId) ? ['required',Rule::unique('Bajet')->ignore($this->NamaBajet, 'NamaBajet')] : 'required|unique:Bajet,NamaBajet',
			'Kategori' => 'required',
			'Nilai' => !empty($this->BajetId) ? '' : 'required',
			'NilaiSemasa' => '',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
