<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class KariahRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'Kariah' => !empty($this->KariahId) ? ['required',Rule::unique('Kariah')->ignore($this->Kariah, 'Kariah')] : 'required|unique:Kariah,Kariah',
			'caka_original_PK' => !empty($this->KariahId) ? ['required',Rule::unique('Kariah')->ignore($this->caka_original_PK, 'caka_original_PK')] : 'required|unique:Kariah,caka_original_PK',
			'KodKariah' => !empty($this->KariahId) ? ['required',Rule::unique('Kariah')->ignore($this->KodKariah, 'KodKariah')] : 'required|unique:Kariah,KodKariah',
			'Penerangan' => !empty($this->KariahId) ? ['required',Rule::unique('Kariah')->ignore($this->Penerangan, 'Penerangan')] : 'required|unique:Kariah,Penerangan',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
