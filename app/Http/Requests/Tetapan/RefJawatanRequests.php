<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RefJawatanRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'RefJawatanId' => '',
			'Kod' => !empty($this->RefJawatanId) ? ['required',Rule::unique('RefJawatan')->ignore($this->Kod, 'Kod')] : 'required|unique:RefJawatan,Kod',
			'Penerangan' => !empty($this->RefJawatanId) ? ['required',Rule::unique('RefJawatan')->ignore($this->Penerangan, 'Penerangan')] : 'required|unique:RefJawatan,Penerangan',
			'Papar' => '',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
