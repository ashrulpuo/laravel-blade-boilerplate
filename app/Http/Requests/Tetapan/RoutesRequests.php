<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RoutesRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'RoutesId' => '',
			'Method' => !empty($this->RoutesId) ? ['required',Rule::unique('Routes')->ignore($this->Method, 'Method')] : 'required|unique:Routes,Method',
			'Uri' => !empty($this->RoutesId) ? ['required',Rule::unique('Routes')->ignore($this->Uri, 'Uri')] : 'required|unique:Routes,Uri',
			'Name' => !empty($this->RoutesId) ? ['required',Rule::unique('Routes')->ignore($this->Name, 'Name')] : 'required|unique:Routes,Name',
			'Controller' => !empty($this->RoutesId) ? ['required',Rule::unique('Routes')->ignore($this->Controller, 'Controller')] : 'required|unique:Routes,Controller',
			'Function' => !empty($this->RoutesId) ? ['required',Rule::unique('Routes')->ignore($this->Function, 'Function')] : 'required|unique:Routes,Function',
			'Namespace' => !empty($this->RoutesId) ? ['required',Rule::unique('Routes')->ignore($this->Namespace, 'Namespace')] : 'required|unique:Routes,Namespace',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
