<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class DunRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'DunId' => '',
			'cadu_original_PK' => !empty($this->DunId) ? ['required',Rule::unique('Dun')->ignore($this->cadu_original_PK, 'cadu_original_PK')] : 'required|unique:Dun,cadu_original_PK',
			'cadu_capa_code' => !empty($this->DunId) ? ['required',Rule::unique('Dun')->ignore($this->cadu_capa_code, 'cadu_capa_code')] : 'required|unique:Dun,cadu_capa_code',
			'cadu_name' => !empty($this->DunId) ? ['required',Rule::unique('Dun')->ignore($this->cadu_name, 'cadu_name')] : 'required|unique:Dun,cadu_name',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
