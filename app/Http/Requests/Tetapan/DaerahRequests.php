<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class DaerahRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'DaerahId' => '',
			'KodDaerah' => !empty($this->DaerahId) ? ['required',Rule::unique('Daerah')->ignore($this->KodDaerah, 'KodDaerah')] : 'required|unique:Daerah,KodDaerah',
			'Penerangan' => !empty($this->DaerahId) ? ['required',Rule::unique('Daerah')->ignore($this->Penerangan, 'Penerangan')] : 'required|unique:Daerah,Penerangan',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
