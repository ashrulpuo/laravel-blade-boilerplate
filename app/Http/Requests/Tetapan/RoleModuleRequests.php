<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RoleModuleRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'RoleModuleId' => '',
			'RoleId' => !empty($this->RoleModuleId) ? ['required',Rule::unique('RoleModule')->ignore($this->RoleId, 'RoleId')] : 'required|unique:RoleModule,RoleId',
			'ModulesId' => !empty($this->RoleModuleId) ? ['required',Rule::unique('RoleModule')->ignore($this->ModulesId, 'ModulesId')] : 'required|unique:RoleModule,ModulesId',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
