<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RefkategoriRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [
            'Kod' => !empty($this->RefKategoriId) ? ['required',Rule::unique('Refkategori')->ignore($this->RefKategoriId, 'RefkategoriId')] : 'required|unique:Refkategori,kod',
            'Penerangan' => !empty($this->RefKategoriId) ? ['required',Rule::unique('Refkategori')->ignore($this->Penerangan, 'Penerangan')] : 'required|unique:Refkategori,Penerangan',
        ];
    }
}
