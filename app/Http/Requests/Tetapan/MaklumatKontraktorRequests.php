<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class MaklumatKontraktorRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'MaklumatKontraktorId' => '',
			'NamaSyarikat' => !empty($this->MaklumatKontraktorId) ? ['required',Rule::unique('MaklumatKontraktor')->ignore($this->NamaSyarikat, 'NamaSyarikat')] : 'required|unique:MaklumatKontraktor,NamaSyarikat',
			'AlamatSyarikat' => !empty($this->MaklumatKontraktorId) ? ['required',Rule::unique('MaklumatKontraktor')->ignore($this->AlamatSyarikat, 'AlamatSyarikat')] : 'required|unique:MaklumatKontraktor,AlamatSyarikat',
			'Negeri' => 'required',
			'Bandar' => 'required',
			'Poskod' => 'required',
			'PersonIncharge' => !empty($this->MaklumatKontraktorId) ? ['required',Rule::unique('MaklumatKontraktor')->ignore($this->PersonIncharge, 'PersonIncharge')] : 'required|unique:MaklumatKontraktor,PersonIncharge',
			'NoSSM' => !empty($this->MaklumatKontraktorId) ? ['required',Rule::unique('MaklumatKontraktor')->ignore($this->NoSSM, 'NoSSM')] : 'required|unique:MaklumatKontraktor,NoSSM',
			'TarikhDaftar' => '',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
