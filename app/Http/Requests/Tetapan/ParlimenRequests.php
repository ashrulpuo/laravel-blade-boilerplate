<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ParlimenRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'ParlimenId' => '',
			'capa_original_PK' => !empty($this->ParlimenId) ? ['required',Rule::unique('Parlimen')->ignore($this->capa_original_PK, 'capa_original_PK')] : 'required|unique:Parlimen,capa_original_PK',
			'capa_cada_code' => !empty($this->ParlimenId) ? ['required',Rule::unique('Parlimen')->ignore($this->capa_cada_code, 'capa_cada_code')] : 'required|unique:Parlimen,capa_cada_code',
			'capa_name' => !empty($this->ParlimenId) ? ['required',Rule::unique('Parlimen')->ignore($this->capa_name, 'capa_name')] : 'required|unique:Parlimen,capa_name',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
