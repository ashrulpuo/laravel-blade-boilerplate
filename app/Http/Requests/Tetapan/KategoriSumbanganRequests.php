<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class KategoriSumbanganRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'KategoriSumbanganId' => '',
			'Penerangan' => !empty($this->KategoriSumbanganId) ? ['required',Rule::unique('KategoriSumbangan')->ignore($this->Penerangan, 'Penerangan')] : 'required|unique:KategoriSumbangan,Penerangan',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
