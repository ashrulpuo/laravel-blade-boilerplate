<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ModuleRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'ModuleId' => '',
			'Module' => !empty($this->ModuleId) ? ['required',Rule::unique('Module')->ignore($this->Module, 'Module')] : 'required|unique:Module,Module',
			'Keterangan' => !empty($this->ModuleId) ? ['required',Rule::unique('Module')->ignore($this->Keterangan, 'Keterangan')] : 'required|unique:Module,Keterangan',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
