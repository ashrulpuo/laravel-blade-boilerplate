<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PenggunaRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'PenggunaId' => '',
			'name' => !empty($this->request->get('PenggunaId')) ? ['required',Rule::unique('Pengguna')->ignore($this->name, 'name')] : '|unique:Pengguna,name',
            'email' => !empty($this->request->get('PenggunaId')) ? ['required',Rule::unique('Pengguna')->ignore($this->email, 'email')] : 'required|unique:Pengguna,email',
            'RefBahagianId' => 'required',
            'JawatanId' => 'required',
			'NoTelPejabat' => '',
			'password' => '',
			'NoTel' => 'required',
			'created_at' => '',
            'updated_at' => '',
            'Penyiasat' => '',
            'RoleId' => 'required',
            'Penyiasat' => '',
            'Password' => ''
        ];
    }
}
