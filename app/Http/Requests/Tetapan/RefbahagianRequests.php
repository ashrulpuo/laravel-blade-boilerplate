<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RefbahagianRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [

			'RefBahagianId' => '',
			'Kod' => !empty($this->RefBahagianId) ? ['required',Rule::unique('RefBahagian')->ignore($this->Kod, 'Kod')] : 'required|unique:RefBahagian,Kod',
			'Penerangan' => !empty($this->RefBahagianId) ? ['required',Rule::unique('RefBahagian')->ignore($this->Penerangan, 'Penerangan')] : 'required|unique:RefBahagian,Penerangan',
			'Papar' => '',
			'created_at' => '',
			'updated_at' => '',
        ];
    }
}
