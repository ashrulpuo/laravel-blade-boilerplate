<?php

namespace App\Models;

use App\Base\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;


class MaklumatPengguna extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'MaklumatPengguna';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $primaryKey = 'MaklumatPenggunaId';

    public const PENYIASAT = '497bfe89-73b5-42c4-8e6e-b609f2bff787';

    protected $fillable = [
        'MaklumatPenggunaId',
        'PenggunaId',
        'BahagianId',
        'JawatanId',
        'NoTel',
        'NoTelPejabat',
        'JawatanId',
        'Penyiasat',
        'Pentadbir',
        'Hapus'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'PenggunaId', 'PenggunaId');
    }

    public function jawatan()
    {
        return $this->hasOne(RefJawatan::class,'RefJawatanId','JawatanId');
    }

    public function bahagian()
    {
        return $this->hasOne(Refbahagian::class,'RefBahagianId','BahagianId');
    }

    // public function role()
    // {
    //     return $this->hasOne(Role::class,'RoleId','RoleId');
    // }
}
