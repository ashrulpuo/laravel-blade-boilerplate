<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\BaseModel;

class Module extends BaseModel
{
    // use \OwenIt\Auditing\Auditable;

    /**
     * enable soft delete
     *
     * @var timestamp
     */
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Module';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ModuleId';

    /**
     * @var array
     */
    protected $fillable = [

		'ModuleId',
		'Module',
		'Keterangan',
		'created_at',
		'updated_at',
    ];

    public function routes()
    {
        return $this->hasMany(ModuleRoute::class, 'ModuleId', 'ModuleId');
    }
}

