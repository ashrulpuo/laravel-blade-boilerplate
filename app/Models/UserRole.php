<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\BaseModel;

class UserRole extends BaseModel
{
    // use \OwenIt\Auditing\Auditable;

    /**
     * enable soft delete
     *
     * @var timestamp
     */
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'UserRole';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'UserRoleId';

    /**
     * @var array
     */
    protected $fillable = [

		'RoleId',
		'PenggunaId',
		'created_at',
		'updated_at',
    ];

    public function role()
    {
        return $this->hasOne(Role::class, 'RoleId', 'RoleId');
    }
}

