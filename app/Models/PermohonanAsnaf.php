<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\BaseModel;

class PermohonanAsnaf extends BaseModel
{
    // use \OwenIt\Auditing\Auditable;

    /**
     * enable soft delete
     *
     * @var timestamp
     */
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'PermohonanAsnaf';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'PermohonanAsnafId';

    /**
     * @var array
     */
    protected $fillable = [

		'Nama',
		'NoKp',
		'NoTel',
        'Email',
		'Jantina',
		'StatusPerkahwinan',
		'Alamat',
		'NegeriId',
		'DaerahId',
		'Poskod',
		'Kariah',
		'Status',
		'PegawaiSemak',
        'Catatan',
		'created_at',
		'updated_at',
    ];

    /**
     * reference to ..
     */
    protected $jantinaGroup = [
        1 => 'Lelaki',
        2 => 'Perempuan'
    ];

    protected $statusPerkahwinanGroup = [
        1 => 'Bujang',
        2 => 'Berkahwin'
    ];

    protected $statusPermohonanGroup = [
        1 => 'Baru',
    ];

    /**
     * append attributes
     */
    protected $appends = [
        'JantinaName',
        'StatusPerkahwinanName',
        'StatusPermohonanName'
    ];

    public function getNameAttribute($attribute, $group)
    {
        if (empty($this->attributes[$attribute])) {
            return '-';
        }

        return $group[$this->attributes[$attribute]];
    }

    public function getJantinaNameAttribute()
    {
        return $this->getNameAttribute('Jantina', $this->jantinaGroup);
    }

    public function getStatusPerkahwinanNameAttribute()
    {
        return $this->getNameAttribute('StatusPerkahwinan', $this->statusPerkahwinanGroup);
    }

    public function getStatusPermohonanNameAttribute()
    {
        switch ($this->attributes['Status']) {
            case 1:
                return '<span class="badge rounded-pill bg-primary">BARU</span>';
                break;
            case 10:
                return '<span class="badge rounded-pill bg-success">DITERIMA</span>';
                break;
            default:
                return '-';
                break;
        }
    }


    /**
     * relastionship table
     */
    public function negeri()
    {
        // Specify the foreign key and local key
        return $this->belongsTo(RefNegeri::class, 'NegeriId', 'RefNegeriId');
    }

    public function daerah()
    {
        return $this->belongsTo(RefDaerah::class, 'DaerahId', 'RefDaerahId');
    }

}

