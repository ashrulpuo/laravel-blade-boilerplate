<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\BaseModel;

class Role extends BaseModel
{
    // use \OwenIt\Auditing\Auditable;

    /**
     * enable soft delete
     *
     * @var timestamp
     */
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Role';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'RoleId';

    /**
     * @var array
     */
    protected $fillable = [

		'RoleId',
		'Role',
		'created_at',
		'updated_at',
    ];

    public const PENGADU = '98897E30-E16B-4C11-9C5D-8596DE81ECBA';

    public function Module()
    {
        return $this->hasMany(RoleModule::class, 'RoleId', 'RoleId');
    }
}

