<?php

namespace App\Models;
use App\Base\BaseModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefNegeri extends BaseModel
{
    use HasFactory;

    // use \OwenIt\Auditing\Auditable;

    /**
     * enable soft delete
     *
     * @var timestamp
     */
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'RefNegeri';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'RefNegeriId';

    /**
     * @var array
     */
    protected $fillable = [
		'Negeri',
		'created_at',
		'updated_at',
    ];
}
