<?php

namespace App\Models;
use App\Base\BaseModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefDaerah extends BaseModel
{
    use HasFactory;

    // use \OwenIt\Auditing\Auditable;

    /**
     * enable soft delete
     *
     * @var timestamp
     */
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'RefDaerah';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'RefDaerahId';

    /**
     * @var array
     */
    protected $fillable = [
        'Daerah',
        'RefNegeriId',
    ];
}
