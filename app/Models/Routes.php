<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\BaseModel;

class Routes extends BaseModel
{
    // use \OwenIt\Auditing\Auditable;

    /**
     * enable soft delete
     *
     * @var timestamp
     */
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Routes';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'RoutesId';

    /**
     * @var array
     */
    protected $fillable = [

		'RoutesId',
		'Method',
		'Uri',
		'Name',
		'Controller',
		'Function',
		'Namespace',
		'created_at',
        'updated_at',
        'Hash'
    ];

}

