<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;

class CreatePermissionRoutes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CreatePermissionRoutes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CreatePermissionRoutes';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $routes = Route::getRoutes()->getRoutes();

        foreach ($routes as $key => $value) {
            dd($value->getName(), $value->getAction()['middleware']['0']);
        }
    }
}
