<?php


// convert base64 to file

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Builder;

function loggedUser()
{
    return auth()->user();
}

function convertBase64ToImage($image_64, $fname = null, $mimes = ['image/png', 'image/jpeg', 'application/pdf'])
{
    $mime       = explode(':', substr($image_64, 0, strpos($image_64, ';')))[1];

    if (!in_array($mime, $mimes)) {
        throw new \Exception('Format MIME ini tidak diterima "' . $mime . '"');
    }

    $extension  = explode('/', $mime)[1];  // .jpg .png .pdf

    if ($extension == "vnd.openxmlformats-officedocument.spreadsheetml.sheet") { // Excel (.xlsx) extension
        $extension  = "xlsx";
    } elseif ($extension == "vnd.openxmlformats-officedocument.wordprocessingml.document") { // Word (.docx) extension
        $extension  = "docx";
    } elseif ($extension == "vnd.openxmlformats-officedocument.presentationml.presentation") { // Powerpoint (.ppt) extension
        $extension  = "pptx";
    } elseif ($extension == "octet-stream") {
        $extension  = "numbers";
    } elseif ($extension == "plain") {
        $extension  = "txt";
    } elseif ($extension == "pdf") {
        $extension  = "pdf";
    }

    $filename   = Str::random(10);

    if (!is_null($fname)) {
        $filename = $fname;
    }

    /**
     * save log
     */
    // try {
    //     $log               = new LogConvertImage();
    //     $log->NamaFail     = $filename . '.' . $extension;
    //     $log->Url          = URL::current();
    //     $log->Base64       = $image_64;
    //     $log->TarikhDaftar = Carbon::now();
    //     $log->save();
    // } catch (Exception $e) {
    //     Log::debug('Error log ' . $filename . '.' . $extension);
    // }
    // $filename
    // $image_64

    $replace    = substr($image_64, 0, strpos($image_64, ',') + 1);
    $image      = str_replace($replace, '', $image_64);
    $image      = str_replace(' ', '+', $image);
    $imageName  = $filename . '.' . $extension;

    return $data = [
        'Filename' => $imageName,
        'Image'    => $image,
        'MIMEType' => $mime
    ];
}

function uploadFile($file) {

    $bytes = random_bytes(20);

    $filename   = \Str::random(10);
    $mime = $file->extension();

    $path = 'public/DokumenAnggaranKontraktor';
    if(!Storage::exists($path)) {
        Storage::makeDirectory($path);
    }

    $file->move(Storage::path($path),$filename . '.' . $mime);

    return $filename . '.' . $mime;
}

function uploadFiles($file, $path) {

    $bytes = random_bytes(20);

    $filename   = \Str::random(10);
    $mime = $file->extension();

    if(!Storage::exists($path)) {
        Storage::makeDirectory($path);
    }

    $file->move(Storage::path($path),$filename . '.' . $mime);

    return $filename . '.' . $mime;
}

function checkPath($path)
{
    if(!Storage::exists($path)) {
        Storage::makeDirectory($path);
    }

    return true;
}

function saveImageStorage($driver, $dir, $base64, $filename, $decode = true)
{
    try {
        if ($decode) {
            $base64 = base64_decode($base64);
        }

        $response = Storage::disk($driver)->put($dir . '/' . $filename, $base64);
        return $response;
    } catch (\Throwable $th) {
        // throw $th;
        Log::error($th);

        return false;
    }
}

function formatHarga($harga)
{
    $harga = str_replace( ',', '', $harga);
    $price_text = strrev($harga); // reverse string
    $arr = str_split($price_text, "3"); // break string in 3 character sets

    $price_new_text = implode(",", $arr);  // implode array with comma
    $price_new_text = strrev($price_new_text); // reverse string back
    $price = str_replace(',.', '.', $price_new_text);

    if (strpos($price, ".") == false) {
        $price = $price . '.00';
    }

    return 'RM ' . $price;
}

function generateMenu($items, $class = '') {
    $html = '';


    foreach ($items as $item) {

        if(empty($item['aduan']) && empty($item['sumbangan'])) {

            $checking = checkRole($item['url']);
            if(!$checking) continue;

            $url = route($item['url']);

            if(\Request::fullUrl() == $url) {
                $active = 'active';
            } else {
                $active = '';
            }
        }



        if(!empty($item['aduan']) && !empty($item['sumbangan'])) {

            if(!empty($item['aduan'])) {
                $sub = '';
                $show = '';
                $aria = 'false';

                foreach ($item['aduan'] as $key => $value) {
                    $checking = checkRole($value['url']);

                    if(!$checking) continue;

                    $url = route($value['url']);

                    if(\Request::fullUrl() == $url) {
                        $active = 'active';
                        $aria = 'true';
                        $show = 'show';
                    } else {
                        $active = '';
                        // $aria = 'false';
                        // $show = '';
                    }

                    $sub .= '<li class="nav-item">
                                <a href="'. $url .'" class="nav-link '.$active.'">'. $value['name'] .'</a>
                            </li>';
                }

                if($sub != '') {
                    if(!empty($item['title'])) {
                        $html .= '<li class="nav-item nav-category">'. $item['title'].'</li>';
                    }

                    $html .= '<li class="nav-item">
                            <a class="nav-link" data-bs-toggle="collapse" href="#'.str_replace(' ', '', $item['title']).'aduan" role="button" aria-expanded="'.$aria.'" aria-controls="'.str_replace(' ', '', $item['title']).'aduan">
                                <i class="link-icon" data-feather="'.$item['icon'].'"></i>
                                <span class="link-title">Aduan</span>
                                <i class="link-arrow" data-feather="chevron-down"></i>
                            </a>
                            <div class="collapse '.$show.'" id="'.str_replace(' ', '', $item['title']).'aduan">
                                <ul class="nav sub-menu">
                                    '. $sub .'
                                </ul>
                            </div>
                        </li>';
                }
            }


            if(!empty($item['sumbangan'])) {
                $subSumbangan = '';
                $sub = '';
                $show = '';
                $aria = 'false';

                foreach ($item['sumbangan'] as $key => $value) {

                    $checking = checkRole($value['url']);
                    if(!$checking) continue;

                    $url = route($value['url']);

                    if(\Request::fullUrl() == $url) {
                        $active = 'active';
                        $aria = 'true';
                        $show = 'show';
                    } else {
                        $active = '';
                        // $aria = 'false';
                        // $show = '';
                    }

                    $subSumbangan .= '<li class="nav-item">
                                        <a href="'. $url .'" class="nav-link '.$active.'">'. $value['name'] .'</a>
                                    </li>';
                }

                if($subSumbangan != '') {

                    $html .= '<li class="nav-item">
                            <a class="nav-link" data-bs-toggle="collapse" href="#'.str_replace(' ', '', $item['title']).'sumbangan" role="button" aria-expanded="'.$aria.'" aria-controls="'.str_replace(' ', '', $item['title']).'sumbangan">
                                <i class="link-icon" data-feather="'.$item['icon'].'"></i>
                                <span class="link-title">Sumbangan</span>
                                <i class="link-arrow" data-feather="chevron-down"></i>
                            </a>
                            <div class="collapse '.$show.'" id="'.str_replace(' ', '', $item['title']).'sumbangan">
                                <ul class="nav sub-menu">
                                    '. $subSumbangan .'
                                </ul>
                            </div>
                        </li>';
                }
            }




        } else {
            if(!empty($item['title'])) {
                $html .= '<li class="nav-item nav-category">'. $item['title'].'</li>';
            }

            $html .= '<li class="nav-item '.$active.'">
                            <a class="nav-link" href="'. $url .'">
                                <i class="link-icon" data-feather="'. $item['icon'] .'"></i>
                                <span class="link-title text-wrap">'. $item['name'] .'</span>
                            </a>
                        </li>';
        }
    }

    return $html;
}

function checkRole($url)
{
    return true;

    $route = Route::current();
    $uri = route($url);
    $lastSegment = parse_url($uri, PHP_URL_PATH);
    $trim = ltrim($lastSegment, '/');
    // dd($lastSegment, session('allowUrl'));
    if (in_array($trim, session('allowUrl'))) {
        return true;
    } else {
        return false;
    }
}

function addSearchQuery($model, $input, $fieldSearchable)
{
    if (!empty($input['search']['value'])) {
        $model->where(function($query) use ($input, $fieldSearchable) {
            foreach ($fieldSearchable as $column) {
                $query->orWhere($column, 'LIKE', '%' . $input['search']['value'] . '%');
            }
        });
    }
    return $model;
}
