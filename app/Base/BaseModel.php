<?php

namespace App\Base;

use App\Builder\SprPaginateBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \App\Http\Traits\AuditTraits;

class BaseModel extends Model
{

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * auto generate UUID
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            /**
             * Get list of attribute
             */
            $attributeArr = count($model->fillable) > 0 ? $model->fillable : $model->getConnection()->getSchemaBuilder()->getColumnListing($model->getTable());

            if (is_null($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = (string) mb_strtoupper(Str::orderedUuid());
            }

            // Checking field is exist in fillable array & no value passed from controller
            if (in_array("created_by", $attributeArr) && !$model->isDirty("created_by")) {
                $model->created_by = Auth::check() ? Auth::user()->PenggunaId : null;
            }
        });
    }

    public static function getIp()
    {
        foreach (array(
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'X_FORWARDED_FOR',
            'HTTP_X_CLUSTER_CLIENT_IP',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'REMOTE_ADDR',
            'SERVER_ADDR'
        ) as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip);
                    // if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }
}
