<?php

return  [
    //dashboard
    [
        'name' => 'Utama',
        'url' => 'home',
        'title' => 'Utama',
        'icon' => 'box',
        'aduan' => [],
        'sumbangan' => [],
    ],
    [
        'name' => 'Permohonan Asnaf',
        'url' => 'home',
        'title' => 'Asnaf',
        'icon' => 'box',
        'aduan' => [],
        'sumbangan' => [],
    ],
    [
        'name' => 'Semakan Permohonan Asnaf',
        'url' => 'semakan-permohonan-asnaf.index',
        'title' => '',
        'icon' => 'box',
        'aduan' => [],
        'sumbangan' => [],
    ],

];
