<?php

namespace spr\generator;

use spr\generator\Console\GenerateCommand;
use Illuminate\Support\ServiceProvider;

class GeneratorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->commands([
            Console\GenerateCommand::class,
            Console\CustomModel::class,
        ]);

        $this->loadViewsFrom(__DIR__ . '/resources/views', 'views');
    }

    public function register()
    {
    }
}
