<?php

namespace Spr\Generator\Console;

use Illuminate\Console\Command;
use DB;

class CustomModel extends Command
{

    protected $signature = 'custom:model {model-name} {table-name}';

    protected $description = 'custom model, usage : custom:model {model-name} {table-name}';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $table = $this->argument('table-name');

        $q = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" . $table . "'";
        $getTables = DB::select($q);

        if (!empty($getTables)) {
            $q = 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ' . "'". $getTables[0]->TABLE_NAME . "'";

            $columns = DB::select($q);
            $newColumns = [];
            foreach ($columns as $key => $value) {
                $newColumns[] = $value->COLUMN_NAME;
            }
            $input = [
                $table . 'Id',
                "LogNamaServer",
                "LogIPServer",
                "DaftarOleh",
                "DaftarPada",
                "LogKemaskiniOleh",
                "LogKemaskiniPada"
            ];
            $result = array_diff($newColumns, $input);
            $model = self::model($result, $table);
            dd($model);
        } else {
            dd('error : table not found');
        }
    }

    public static function model($set, $table)
    {
        $content = view('views::_model_template', ['set' => $set, 'table' => $table]);
        $path = base_path();
        $modelFile = $path . '/app/Models/' . $table . '.php';
        if (!file_exists($modelFile)) {
            if (file_put_contents($modelFile, $content) !== false) {
                return ['success' => "Model created (" . basename($modelFile) . ")"];
            }
        }

        return "model exists";
    }
}
