<?php

namespace spr\generator\Console;
use Illuminate\Console\Command;
use DB;

class GenerateCommand extends Command {

    protected $signature = 'tetapan:generate {table}';

    protected $description = 'crud generator usage "php artisan tetapan:generator RefWargaSpr"';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $tb = $this->argument('table');
        // $q = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" . $tb . "'";

        // $getTables = DB::select($q);

        $getTables = DB::select('SHOW TABLES');

        $tables = [];
        foreach ($getTables as $key => $value) {
            if($value->Tables_in_maim != $tb) continue;
            $tables[] = $tb;
        }

        if(empty($tables)) dd('Tiada maklumat table');

        // CreateModel::web($tables);
        CreateModel::GetColumns($tables);
    }

}
