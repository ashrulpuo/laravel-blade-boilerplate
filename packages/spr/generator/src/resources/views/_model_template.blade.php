<?=
"
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\BaseModel;

class " . $table . " extends BaseModel
{
    // use \OwenIt\Auditing\Auditable;

    /**
     * enable soft delete
     *
     * @var timestamp
     */
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected \$table = '". $table ."';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected \$primaryKey = '". $table.'Id' ."';

    /**
     * @var array
     */
    protected \$fillable = [
"?>
<?php
    foreach ($set as $i => $column) {
        echo "\n\t\t'" . $column . "',";
    }
?>
<?="
    ];

}

"
?>
