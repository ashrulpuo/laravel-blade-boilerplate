<?=
"
<?php

namespace App\Http\Requests\Tetapan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ". $table.'Requests' ." extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //check request for unique value
        return [
"?>
<?php
    foreach ($set as $i => $column) {
        if (in_array($column, ['created_at','updated_at', 'Papar','email_verified_at', 'password', 'remember_token', $table . 'Id'])) {
            echo "\n\t\t\t'$column' => '',";
        } else {
            echo "\n\t\t\t'$column' => !empty(\$this->".$table.'Id'.") ? ['required',Rule::unique('". $table ."')->ignore(\$this->". $column .", '" . $column . "')] : 'required|unique:$table,$column',";
        }
    }
?>
<?="
        ];
    }
}
"
?>
