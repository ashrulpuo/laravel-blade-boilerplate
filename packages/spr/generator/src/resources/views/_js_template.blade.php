<?=
"
$(function() {

var statusCode;

var table = $('#kt_datatable')

var ". $table ."DT = table.DataTable({
    responsive: true,
    searchDelay: 500,
    processing: true,
    serverSide: true,
    scrollCollapse: true,
    pagingType: 'full_numbers',
    ordering: false,
    responsive: true,
    language: {
        search: \"Carian :\"
    },
    ajax: {
        url: table.data('route'),
        method: 'GET',
        dataType: 'json',
        dataSrc: \"data\",
    },
    columns: [
"?>
<?php
    foreach ($set as $i => $column) {
        if (in_array($column, ['created_at','updated_at', 'Papar','email_verified_at','password', 'remember_token'])) continue;

        if($column == $table.'Id'){
            echo "\t\t{
            title: '#',
            data: '" . $column . "',
            render: function(data, type, full, meta) {
                return meta.row + 1;
            }
        },";
        } else {
            if (!in_array($column, ['created_at','updated_at', 'Papar', $table . 'Id'])) {
                echo "\n\t\t{
            title: '" . ucwords($column) . "',
            data: '" . $column . "'
        },";
            } else {
                echo "\n\t\t{
            title: '" . ucwords($column) . "',
            data: '" . $column . "',
            render: function(row) {
                return moment(row).format('DD/MM/YYYY');
            },
        },";
            }
        }
    }
?>
<?php
foreach ($set as $i => $column) {
    if($column == $table.'Id'){
        echo "\n\t\t{
            width: '75px',
            targets: -1,
            data: '" . $column . "',
            title: 'Tindakan',
            orderable: false,
            render: function(data, type, full, meta) {
                return `\
                    <a href=\"javascript:;\" id=\"\${full.".$column."}\" class=\"edit\">\
                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg>\
                    </a>\
                    <a href=\"javascript:;\" id=\"\${full.".$column."}\" class=\"delete\" style=\"color:red\">\
                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2'><polyline points='3 6 5 6 21 6'></polyline><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg>\
                    </a>\
                `;
            },
        },";
    }
}
?>
<?="
    ],
});

table.each(function() {
    var datatable = $(this);
    // SEARCH - Add the placeholder for Search and Turn this into in-line form control
    var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
    search_input.attr('placeholder', 'Search');
    search_input.removeClass('form-control-sm');
    // LENGTH - Inline-Form control
    var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
    length_sel.removeClass('form-control-sm');
});

$('body').on('click', '.create', function() {
    $('#tetapan" .$table. "')[0].reset();
    $('#modelHeading').html(\"Tetapan ".$table."\" );

    $('#modelHeading').html('Daftar');
    $('#simpan').html('Hantar');
    $('.modal').modal('show');
});

//submit
$('#simpan').click(function(e) {
    // e.preventDefault();

    let method;
    let url;

    var id = $(\"#". $table."Id" ."\").val();

    if (id === '' || id === undefined) {
        url = table.data('route');
        method = \"POST\";
    } else {
        url = table.data('route') + '/' + id;
        method = \"PUT\";
    }

    $.ajax({
        data: $('#tetapan".$table."').serialize(),
        url: url,
        type: method,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content')
        },
        success: ((data) => {
            statusCode = 1;
            window.helper.success(data, statusCode);
            ". $table ."DT.draw();
        }),
        error: function(data) {
            window.helper.error(data)
        }
    });
});

//edit
$('body').on('click', '.edit', function() {
    var ". $table.'Id' ." = $(this).attr('id');
    var route = table.data('route') + '/' + ". $table.'Id' ." + '/edit';

    $.get(route, function(data) {
"?>
<?php
foreach ($set as $i => $column) {
    if (!in_array($column, ['created_at','updated_at', 'Papar','email_verified_at','password','remember_token'])) {
        echo "\t\t$('#". $column ."').val(data.data.". $column .");"."\r\n";
    }

    if($column == 'Papar'){
        echo "\t\t$(\"#Papar\").prop(\"checked\", (data.data.Papar === 1) ? true : false);"."\r\n";
    }
}
?>
<?="
        $('#modelHeading').html(\"Kemaskini\");
        $('#simpan').html(\"Kemaskini\" );
        $('.modal').modal('show');
    })
});

//delete
$('body').on('click', '.delete', function() {
    var ". $table.'Id' ." = $(this).attr('id');
    var url = table.data('route') + '/' + ". $table.'Id' .";

    Swal.fire({
        title: 'NOTIFIKASI',
        text: 'Adakah anda pasti?',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#038cfc',
        cancelButtonColor: '#999',
        confirmButtonText: 'YA',
        cancelButtonText: 'BATAL',
        allowOutsideClick: false
    }).then((response) => {
        if (response.value) {
              $.ajax({
                type: \"DELETE\",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content')
                },
                success: function(data) {
                    statusCode = 2;
                    window.helper.success(data, statusCode)
                    ". $table ."DT.draw();
                },
                error: function(data) {
                    $('#alert').removeClass('alert-success').addClass('alert-danger');
                    $('#icon').removeClass('flaticon2-check-mark').addClass('flaticon2-delete');
                    $(\"#text\").html(data.message);
                    $(\".alert\").delay(3000).fadeOut();
                    ". $table ."DT.draw();
                }
            });
        }
    });
});
});
"
?>
