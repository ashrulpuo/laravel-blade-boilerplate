@extends('layout.landing_layout')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

<div class="inner-banner thm-black-bg text-center">
    <div class="container">
        <h2 class="inner-banner__title">Permohonan Asnaf</h2><!-- /.inner-banner__title -->
    </div><!-- /.container -->
</div><!-- /.inner-banner -->
<div class="contact-block-one sec-pad-content-margin-50">
    <div class="container">
        @include('common.alert')

        <div class="block-title text-center">
            <p class="block-title__tag-line">Sila isi maklumat anda</p>
        </div><!-- /.block-title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="content-margin-50">
                    <form class="forms-sample" method="POST" id="permohonanAsnafFormId" action="{{ route('permohonan-akaun.store') }}">
                        @csrf
                        <div class="auth-form-wrapper px-4 py-5">
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">Nama Penuh</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="Nama" id="Nama" placeholder="Nama Penuh">
                                    @error('Nama')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">No. Kad Pengenalan</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="NoKp" id="NoKp" placeholder="No. Kad Pengenalan">
                                    @error('NoKp')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <hr>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">E-mel</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="Email" id="Email" placeholder="E-mail Permohon">
                                    @error('Email')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">No. Telefon</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" maxlength="11" name="NoTel" id="NoTel" placeholder="No. Telefon">
                                    @error('NoTel')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">Jantina</label>
                                <div class="col-sm-9">
                                    <select name="Jantina" class="form-select form-control" aria-label="Default select example">
                                        <option value="" selected>Sila Pilih</option>
										<option value="1">Lelaki</option>
										<option value="2">Perempuan</option>
									</select>
                                    @error('Jantina')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">Status Perkahwinan</label>
                                <div class="col-sm-9">
                                    <select name="StatusPerkahwinan" class="form-select form-control" aria-label="Default select example">
                                        <option value="" selected>Sila Pilih</option>
										<option value="1">Bujang</option>
										<option value="2">Berkahwin</option>
									</select>
                                    @error('StatusPerkahwinan')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">Alamat Terkini</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="Alamat" id="Alamat" placeholder="Alamat Terkini">
                                    @error('Alamat')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">Negeri</label>
                                <div class="col-sm-9">
                                    <select id="negeriSelect" name="NegeriId" class="form-select form-control" aria-label="Default select example">
                                        <option value="" selected>Sila Pilih</option>
										@foreach ($negeris as $negeri)
                                        <option value="{{ $negeri->RefNegeriId }}">{{ $negeri->Negeri }}</option>
                                        @endforeach
                                        @error('NegeriId')
                                            <span class="" style="color: red;">{{ $message }}</span>
                                        @enderror
									</select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">Daerah</label>
                                <div class="col-sm-9">
                                    <select id="daerahSelect" name="DaerahId" class="form-select form-control" aria-label="Default select example">
                                        <option value="" disabled selected>Sila Pilih</option>
									</select>
                                    @error('DaerahId')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">Poskod</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="Poskod" maxlength="5" id="Poskod" placeholder="Poskod">
                                    @error('Poskod')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="userEmail" class="col-sm-3 form-label">Kariah Terdekat</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="Kariah" id="Kariah" placeholder="Kariah Terdekat">
                                    @error('Kariah')
                                        <span class="" style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button id="btnDaftar" type="submit" class="float-end btn btn-success mr-2">DAFTAR</button>
                    </form>
                </div><!-- /.contact-block-one_form -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.contact-block-one -->

@endsection


@section('js')
<script src="{{ asset('asset/backend/backend.bundle.js') }}"></script>
<script>
// A $( document ).ready() block.
$( document ).ready(function() {
    $('#negeriSelect').on('change', function() {
        var id = $(this).val();
        var base_url = window.location.origin;

        $.ajax({
            url: base_url + '/tetapan/daerah/' + id,
            type: "get",
            dataType: 'json',
            success: ((data) => {
                $('#daerahSelect').empty();
                $('#daerahSelect').append(`<option value="" disabled selected>Sila Pilih</option>`);

                $.each(data.daerahs, function (i, item) {
                    $('#daerahSelect').append($('<option>', {
                        value: item.RefDaerahId,
                        text: item.Daerah,
                    }));
                });
            }),
            error: function (data) {
                Swal.fire(
                    'NOTIFIKASI',
                    'Terdapat ralat pada data daerah',
                    'error'
                )
            }
        });
    })

    $("#permohonanAsnafFormId" ).validate({
        rules: {
            Nama: { required: true },
            NoKp : { required: true, number:true, minlength: 12, maxlength: 12 },
            Email : { required: true, email: true },
            NoTel : { required: true, number: true },
            Jantina : { required: true },
            StatusPerkahwinan : { required: true },
            Alamat : { required: true },
            NegeriId : { required: true },
            DaerahId : { required: true },
            Poskod : { required: true, number: true },
            Kariah : { required: true },
        },
        messages: {
            required: "Sila isi maklumat ini", // Common message for all required fields
        },
        errorPlacement: function(error, element) {
            error.addClass( "invalid-feedback" );

            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                error.insertAfter(element.parent().parent());
            } else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            }
        },
        unhighlight: function(element, errorClass) {
            if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

});
</script>
@endsection

