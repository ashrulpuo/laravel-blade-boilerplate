@extends('layout.app')

@section('css')
@endsection

@section('content')
    <nav class='page-breadcrumb'>
        <ol class='breadcrumb'>
            <li class='breadcrumb-item'>Permohonan Asnaf</li>
            <li class='breadcrumb-item active' aria-current='page'>Maklumat Pemohon</li>
        </ol>
    </nav>

    <div class='row'>
        <div class='col-lg-12'>
            @include('common.alert')

            <div class='card'>
                <div class='card-header'>
                    <h4 class='card-title'>Permohonan Asnaf</h4>
                    <p class='card-title-desc'>Maklumat Pemohon</p>
                </div>
                <div class='card-body'>
                    <div class="col-md-12">
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">Nama Pemohon :</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ strtoupper($permohonan->Nama) }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">NoKp Pemohon:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ $permohonan->NoKp }}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">E-mal:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ $permohonan->Email }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">No Tel:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ strtoupper($permohonan->NoTel) }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">Jantina:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ strtoupper($permohonan->jantinaName) }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">Status Perkahwinan:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ strtoupper($permohonan->statusPerkahwinanName) }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">Alamat:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ strtoupper($permohonan->Alamat) }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">Negeri:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ strtoupper($permohonan->negeri->Negeri) }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">Daerah:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ strtoupper($permohonan->daerah->Daerah) }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">Poskod:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ strtoupper($permohonan->Poskod) }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">Kariah:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{{ strtoupper($permohonan->Kariah) }}</p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <p class="text-dark font-weight-bold">Status Permohonan:</p>
                            </div>
                            <div class="col-md-10">
                                <p>{!! $permohonan->StatusPermohonanName !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </br>
            <div class="card">
                <form action="{{ route('semakan-permohonan-asnaf.update', $permohonan->PermohonanAsnafId) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="card-header">
                        <h4 class="card-title">Tindakan Pegawai</h4>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Tindakan</p>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" value="10" name="Tindakan" id="terima">
                                        <label class="form-check-label" for="terima">Terima</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" value="20" name="Tindakan" id="tolak">
                                        <label class="form-check-label" for="tolak">Tolak</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Catatan</p>
                                </div>
                                <div class="col-md-10">
                                    <textarea id="maxlength-textarea" class="form-control" maxlength="1000" rows="3" name="Catatan" placeholder="Ulasan penyiasat."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-transparent">
                        <div class="form-group row">
                            <div class="col">
                                <a href="{{ route('semakan-permohonan-asnaf.index') }}" type="button" class="btn btn-danger btn-xs"">KEMBALI KE SENARAI</a>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-success btn-xs">HANTAR</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    $(document).ready(function() {

    });
</script>
@endsection
