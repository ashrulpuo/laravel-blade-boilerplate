<!-- using html -->
@if(Session::get('type'))
<div class="alert alert-{{ Session::get('type') }} alert-dismissible" role="alert">
    <i data-feather="{{ Session::get('icon') }}"></i>
    <strong id="text">{{ Session::get('message') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close"></button>
</div>
@endif

<div class="alert alert-success alert-dismissible fade show" style="display:none" role="alert">
    <i data-feather="check-circle"></i>
    <strong id="text"></strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close"></button>
</div>
