<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Responsive HTML Admin Dashboard Template based on Bootstrap 5">
	<meta name="author" content="NobleUI">
	<meta name="keywords" content="nobleui, bootstrap, bootstrap 5, bootstrap5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

	<title>Zakat Fintech</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <!-- End fonts -->
    <link href="{{ asset('asset/backend/backend.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/backend/datatables.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('asset/plugins/select2/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://fonts.cdnfonts.com/css/feather" rel="stylesheet">
    <style>
        label.error {
            color: red;
        }

        .select2-container .select2-selection--single {
            height: 32px !important
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            padding: -0.531rem 0.8rem !important
        }

        .text-wrap{
            white-space:normal;
        }
        .width-200{
            width:500px;
        }
    </style>
  @yield('css')
</head>
<body class="sidebar-dark">
	<div class="main-wrapper">

		<!-- partial:partials/_sidebar.html -->
        @include('layout.side-bar')
		<!-- partial -->

		<div class="page-wrapper">

			<!-- partial:partials/_navbar.html -->
			@include('layout.nav-bar')
			<!-- partial -->

        <div class="page-content">
            @yield('content')
        </div>

			<!-- partial:partials/_footer.html -->
			@include('layout.footer')
			<!-- partial -->

		</div>
	</div>

    <script src="{{ asset('js/appv2.js') }}"></script>
    <script src="{{ asset('asset/backend/backend.bundle.js') }}"></script>
    <script src="{{ asset('asset/backend/datatables.bundle.js') }}"></script>
    <script src="{{ asset('asset/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment-with-locales.min.js"></script>
    <script src="{{ asset('asset/plugins/select2/select2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js" integrity="sha512-Rdk63VC+1UYzGSgd3u2iadi0joUrcwX0IWp2rTh6KXFoAmgOjRS99Vynz1lJPT8dLjvo6JZOqpAHJyfCEZ5KoA==" crossorigin="anonymous"></script>

    <script>
        $( document ).ready(function() {

            $('.currency').maskMoney({thousands: ',', decimal: '.', allowZero: true, allowNegative: true});

            if ($(".select2").length) {
                $(".select2").select2({
                    allowClear: true,
                });
            }

            if ($(".js-example-basic-multiple").length) {
                $(".js-example-basic-multiple").select2();
            }

            if($('.datepicker').length) {
                var date = new Date();
                var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    todayHighlight: true,
                    autoclose: true
                });
                $('.datepicker').datepicker('setDate', today);
            }

            function numOnly(selector){
                selector.value = selector.value.replace(/[^0-9]/g,'');
            }

            if($('#flatpickr-date').length) {
                flatpickr("#flatpickr-date", {
                    wrap: true,
                    todayHighlight: true,
                    autoclose: true,
                    dateFormat: "d/m/Y",
                    defaultDate: "today",
                });
            }


            // time picker
            if($('#flatpickr-time').length) {
                flatpickr("#flatpickr-time", {
                wrap: true,
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                });
            }
        });
    </script>
    @yield('js')
</body>
</html>
