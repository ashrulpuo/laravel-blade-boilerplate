<nav class="sidebar">
    <div class="sidebar-header">
        <a href="#" class="sidebar-brand">
            <span class="logo-lg d-flex justify-content-center">
                {{-- <img src="{{ asset('images/maim01.jpg') }}" alt="" height="26"> <div style="font-size: 12px">Sistem Aduan  </div> --}}
            </span>
        </a>
        <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">
           {!! generateMenu(config('menu'), 'main-menu') !!}
        </ul>
    </div>
</nav>
