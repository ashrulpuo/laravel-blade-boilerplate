<footer class="site-footer">
    <div class="main-footer">
        <div class="container">
            <div class="inner-container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="footer-widget contact-widget">
                            <div class="widget-title">
                                <h3>Instagram</h3>
                            </div><!-- /.widget-title -->
                            <ul class="insta-photo-list list-unstyled">
                                <li>
                                    <a href="#"><img src="{{ asset('landing/images/resources/insta-1-1.jpg') }}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{ asset('landing/images/resources/insta-1-2.jpg') }}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{ asset('landing/images/resources/insta-1-3.jpg') }}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{ asset('landing/images/resources/insta-1-4.jpg') }}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{ asset('landing/images/resources/insta-1-5.jpg') }}" alt=""></a>
                                </li>
                                <li>
                                    <a href="#"><img src="{{ asset('landing/images/resources/insta-1-6.jpg') }}" alt=""></a>
                                </li>
                            </ul><!-- /.insta-photo-list -->
                            <!-- /.insta-photo-list -->
                        </div><!-- /.footer-widget -->
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="footer-widget contact-widget">
                            <div class="widget-title">
                                <h3>Contact</h3>
                            </div><!-- /.widget-title -->
                            <p>666 888 0000</p>
                            <p>needhelp@example.com</p>
                            <p>66 Road Broklyn Street, 600 <br> New York, USA</p>
                        </div><!-- /.footer-widget -->
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-2">
                        <div class="footer-widget">
                            <div class="widget-title">
                                <h3>Explore</h3>
                            </div><!-- /.widget-title -->
                            <ul class="link-list">
                                <li><a href="#">About</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Our Services</a></li>
                                <li><a href="#">Plans & Pricing</a></li>
                                <li><a href="#">How it Works</a></li>
                            </ul><!-- /.link-list -->
                        </div><!-- /.footer-widget -->
                    </div><!-- /.col-lg-2 -->
                    <div class="col-lg-4">
                        <div class="footer-widget">
                            <div class="widget-title">
                                <h3>About Dexen</h3>
                            </div><!-- /.widget-title -->
                            <p>Lorem ipsum dolor sit amet, consectetur adip sicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.</p>
                            <div class="social-block">
                                <a href="#"><i class="fa fa-facebook-f"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-youtube-play"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div><!-- /.social-block -->
                        </div><!-- /.footer-widget -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.inner-container -->
        </div><!-- /.container -->
    </div><!-- /.main-footer -->
    <div class="bottom-footer text-center">
        <div class="container">
            <div class="inner-container">
                <p>&copy; All copyrights are reserved. 2019 <a href="#">TonaTheme.com</a></p>
            </div><!-- /.inner-container -->
        </div><!-- /.container -->
    </div><!-- /.bottom-footer -->
</footer><!-- /.site-footer -->
