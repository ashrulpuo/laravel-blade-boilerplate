<div class="top-bar">
    <div class="container">
        <div class="social-block">

        </div><!-- /.social-block -->
        <div class="logo-block">
            <a href="index.html"><img src="{{  asset('landing/images/resources/logo-1-1.png') }}" alt="Awesome Image" /></a>
        </div><!-- /.logo-block -->
        <div class="right-block">
            <a href="#"><i class="fa fa-envelope"></i>needhelp@example.com</a>
            <a href="#"><i class="fa fa-phone-square"></i>666 888 0000</a>
        </div><!-- /.right-block -->
    </div><!-- /.container -->
</div><!-- /.top-bar -->
