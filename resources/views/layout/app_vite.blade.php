<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="Responsive HTML Admin Dashboard Template based on Bootstrap 5">
	<meta name="author" content="NobleUI">
	<meta name="keywords" content="nobleui, bootstrap, bootstrap 5, bootstrap5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">

	<title>Sistem Aduan (MAIM)</title>

  <!-- Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
  <!-- End fonts -->


  @vite([
    //'resources/css/app.css',
    'resources/js/app.js',
    'resources/js/vendors/flag-icon-css/css/flag-icon.min.css',
    'resources/fonts/feather-font/css/iconfont.css',
    'resources/js/vendors/flatpickr/flatpickr.min.css',
    'resources/css/style.css',
    'resources/js/vendors/core/core.css',
    'resources/js/vendors/datatables.net-bs5/dataTables.bootstrap5.css',
    'resources/js/vendors/sweetalert2/sweetalert2.min.css'
  ])

  @yield('css')
</head>
<body class="sidebar-dark">
	<div class="main-wrapper">

		<!-- partial:partials/_sidebar.html -->
        @include('layout.side-bar')
		<!-- partial -->

		<div class="page-wrapper">

			<!-- partial:partials/_navbar.html -->
			@include('layout.nav-bar')
			<!-- partial -->

        <div class="page-content">
            @yield('content')
        </div>

			<!-- partial:partials/_footer.html -->
			@include('layout.footer')
			<!-- partial -->

		</div>
	</div>

    <script type="module" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.2/jquery.min.js" integrity="sha512-tWHlutFnuG0C6nQRlpvrEhE4QpkG1nn2MOUMWmUeRePl4e3Aki0VB6W1v3oLjFtd0hVOtRQ9PHpSfN6u6/QXkQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="module" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.2/jquery.js" integrity="sha512-NMtENEqUQ8zHZWjwLg6/1FmcTWwRS2T5f487CCbQB3pQwouZfbrQfylryimT3XvQnpE7ctEKoZgQOAkWkCW/vg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="module" src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<!-- core:js -->
    @vite([
        'resources/js/vendors/core/core.js',
        'resources/js/vendors/flatpickr/flatpickr.min.js',
        'resources/js/vendors/feather-icons/feather.min.js',
        'resources/js/dist/template.js',
        'resources/js/vendors/apexcharts/apexcharts.min.js',
        'resources/js/vendors/datatables.net/jquery.dataTables.js',
        'resources/js/vendors/datatables.net-bs5/dataTables.bootstrap5.js',
        'node_modules/jquery.repeater/jquery.repeater.js',
        'node_modules/jquery.repeater/jquery.repeater.min.js',
        'resources/js/vendors/sweetalert2/sweetalert2.min.js'
    ])

    @yield('js')
</body>
</html>
