<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home | Zakat Fintech</title>
    <link rel="apple-touch-icon" sizes="57x57" href="{{  asset('landing/images/favicon//apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{  asset('landing/images/favicon//apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{  asset('landing/images/favicon//apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{  asset('landing/images/favicon//apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{  asset('landing/images/favicon//apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{  asset('landing/images/favicon//apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{  asset('landing/images/favicon//apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{  asset('landing/images/favicon//apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{  asset('landing/images/favicon//apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{  asset('landing/images/favicon//android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{  asset('landing/images/favicon//favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{  asset('landing/images/favicon//favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{  asset('landing/images/favicon//favicon-16x16.png') }}">
    <link rel="manifest" href="{{  asset('landing/images/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{  asset('landing/css/style.css') }}">
    <link rel="stylesheet" href="{{  asset('landing/css/responsive.css') }}">
</head>

<body>
    <div class="preloader"></div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one">
            @include('layout.landing.sub_header')

            @include('layout.landing.landing_nav')
        </header><!-- /.site-header header-one -->

        @yield('content')

        @include('layout.landing.landing_footer')
    </div><!-- /.page-wrapper -->
    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
    <!-- /.scroll-to-top -->
    <script src="{{  asset('landing/js/jquery.js') }}"></script>
    <script src="{{  asset('landing/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{  asset('landing/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{  asset('landing/js/owl.carousel.min.js') }}"></script>
    <script src="{{  asset('landing/js/swiper.min.js') }}"></script>
    <script src="{{  asset('landing/js/theme.js') }}"></script>


    @yield('js')
</body>

</html>
