<nav class="sidebar">
    <div class="sidebar-header">
      <a href="#" class="sidebar-brand">
        <span class="logo-lg d-flex justify-content-center">
          {{-- <img src="{{ asset('images/maim01.jpg') }}" alt="" height="26"> <div style="font-size: 12px">Sistem Aduan  </div> --}}
      </span>
      </a>
      <div class="sidebar-toggler not-active">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">
            <li class="nav-item nav-category">Utama</li>
            <li class="nav-item {{ (\Request::route()->getName() == 'home') ? 'active' : '' }}">
            <a href="{{ url('/home') }}" class="nav-link">
                <i class="link-icon" data-feather="box"></i>
                <span class="link-title">Dashboard</span>
            </a>
            </li>

            <li class="nav-item nav-category">Aduan</li>
            <li class="nav-item {{ (\Request::route()->getName() == 'pendaftaran-aduan') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('pendaftaran-aduan') }}">
                    <i class="link-icon" data-feather="message-square"></i>
                    <span class="link-title">Borang Aduan</span>
                </a>
            </li>
            <li class="nav-item {{ (\Request::route()->getName() == 'senarai-aduan') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('senarai-aduan') }}">
                    <i class="link-icon" data-feather="align-center"></i>
                    <span class="link-title">Aduan Saya</span>
                </a>
            </li>

            {{-- @if(Auth::user()->MaklumatPengguna->RoleId != 'cb438f99-78e3-460f-ab7e-0046fec75584') --}}

                {{-- @if(in_array(Auth::user()->MaklumatPengguna->RoleId, ['9513117b-2cbd-4a90-89fb-1989c359d205', 'b4b70e47-6778-4dbd-8f35-558b166e1e8c'])) --}}
                    <li class="nav-item nav-category">Pegawai</li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'senarai.pegawai-semak') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.pegawai-semak') }}">
                            <i class="link-icon" data-feather="align-center"></i>
                            <span class="link-title">Senarai Belum Disemak</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'carian-aduan') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('carian-aduan') }}">
                            <i class="link-icon" data-feather="search"></i>
                            <span class="link-title">Carian Aduan</span>
                        </a>
                    </li>
                {{-- @endif --}}

                {{-- @if(in_array(Auth::user()->MaklumatPengguna->RoleId, ['497bfe89-73b5-42c4-8e6e-b609f2bff787', '9513117b-2cbd-4a90-89fb-1989c359d205'])) --}}
                    <li class="nav-item nav-category">Penyiasat</li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'senarai.penyiasat-semak') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.penyiasat-semak') }}">
                            <i class="link-icon" data-feather="list"></i>
                            <span class="link-title">Senarai Semakan Aduan</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'senarai.penyiasat-telah-disemak') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.penyiasat-telah-disemak') }}">
                            <i class="link-icon" data-feather="list"></i>
                            <span class="link-title text-wrap">Senarai Selesai Semakan Siasatan</span>
                        </a>
                    </li>

                    {{-- <li class="nav-item {{ (\Request::route()->getName() == 'sumbangan.siasatan.senaraiSiasatanSumbangan') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('sumbangan.siasatan.senaraiSiasatanSumbangan') }}">
                            <i class="link-icon" data-feather="eye"></i>
                            <span class="link-title text-wrap">Senarai Permohonan Sumbangan</span>
                        </a>
                    </li> --}}
                {{-- @endif --}}


                {{-- @if(in_array(Auth::user()->MaklumatPengguna->RoleId, ['83a43ec2-aa5c-4323-bdbc-a270b862d61c', '9513117b-2cbd-4a90-89fb-1989c359d205'])) --}}
                    <li class="nav-item nav-category">Ketua Unit</li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'senarai.ketua-unit-semak') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.ketua-unit-semak') }}">
                            <i class="link-icon" data-feather="check-circle"></i>
                            <span class="link-title">Senarai Semakan Aduan</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'senarai.ku-telah-disemak') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.ku-telah-disemak') }}">
                            <i class="link-icon" data-feather="list"></i>
                            <span class="link-title text-wrap">Senarai Selesai Semakan</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'senarai.ku.kiv') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.ku.kiv') }}">
                            <i class="link-icon" data-feather="book"></i>
                            <span class="link-title text-wrap">Senarai Aduan KIV</span>
                        </a>
                    </li>
                    {{-- <li class="nav-item {{ (\Request::route()->getName() == 'sumbangan.siasatan.senaraiSiasatanSumbanganKU') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('sumbangan.siasatan.senaraiSiasatanSumbanganKU') }}">
                            <i class="link-icon" data-feather="trash-2"></i>
                            <span class="link-title text-wrap">Senarai Permohonan Sumbangan</span>
                        </a>
                    </li> --}}
                {{-- @endif --}}

                {{-- @if(in_array(Auth::user()->MaklumatPengguna->RoleId, ['b4b70e47-6778-4dbd-8f35-558b166e1e8c', '9513117b-2cbd-4a90-89fb-1989c359d205'])) --}}
                    <li class="nav-item nav-category">PSU</li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'senarai.psu-semak') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.psu-semak') }}">
                            <i class="link-icon" data-feather="clipboard"></i>
                            <span class="link-title">Senarai Semakan Aduan</span>
                        </a>
                    </li>
                    {{-- <li class="nav-item {{ (\Request::route()->getName() == 'senarai.psu.kiv') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.psu.kiv') }}">
                            <i class="link-icon" data-feather="book"></i>
                            <span class="link-title text-wrap">Senarai Aduan KIV</span>
                        </a>
                    </li> --}}
                    <li class="nav-item {{ (\Request::route()->getName() == 'senarai.psu.senaraiDitolak') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.psu.senaraiDitolak') }}">
                            <i class="link-icon" data-feather="book"></i>
                            <span class="link-title text-wrap">Senarai Aduan Ditolak</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'senarai.psu.senaraiLulus') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('senarai.psu.senaraiLulus') }}">
                            <i class="link-icon" data-feather="book"></i>
                            <span class="link-title text-wrap">Senarai Aduan Selesai</span>
                        </a>
                    </li>
                    {{-- <li class="nav-item {{ (\Request::route()->getName() == 'sumbangan.siasatan.senaraiSiasatanSumbanganBaruPSU') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('sumbangan.siasatan.senaraiSiasatanSumbanganBaruPSU') }}">
                            <i class="link-icon" data-feather="book"></i>
                            <span class="link-title text-wrap">Senarai Sumbangan Baru</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'sumbangan.siasatan.senaraiSiasatanSumbanganPSU') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('sumbangan.siasatan.senaraiSiasatanSumbanganPSU') }}">
                            <i class="link-icon" data-feather="book"></i>
                            <span class="link-title text-wrap">Senarai Permohonan Sumbangan</span>
                        </a>
                    </li> --}}

                    <li class="nav-item nav-category">Kerani</li>
                    {{-- <li class="nav-item {{ (\Request::route()->getName() == 'pengurusan-sebut-harga.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('pengurusan-sebut-harga.index') }}">
                            <i class="link-icon" data-feather="dollar-sign"></i>
                            <span class="link-title">Daftar sebut harga</span>
                        </a>
                    </li> --}}
                    <li class="nav-item {{ (\Request::route()->getName() == 'pengurusan-mesyuarat.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('pengurusan-mesyuarat.index') }}">
                            <i class="link-icon" data-feather="briefcase"></i>
                            <span class="link-title">Kalendar mesyuarat</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'prestasi-kontraktor.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('prestasi-kontraktor.index') }}">
                            <i class="link-icon" data-feather="activity"></i>
                            <span class="link-title">Prestasi Kontraktor</span>
                        </a>
                    </li>

                    <li class="nav-item nav-category">Tetapan</li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'Pengguna.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('Pengguna.index') }}">
                            <i class="link-icon" data-feather="users"></i>
                            <span class="link-title">Pengguna</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'kategori.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('kategori.index') }}">
                            <i class="link-icon" data-feather="settings"></i>
                            <span class="link-title">Kategori</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'Refbahagian.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('Refbahagian.index') }}">
                            <i class="link-icon" data-feather="settings"></i>
                            <span class="link-title">Bahagian</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'RefJawatan.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('RefJawatan.index') }}">
                            <i class="link-icon" data-feather="settings"></i>
                            <span class="link-title">Jawatan</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'MaklumatKontraktor.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('MaklumatKontraktor.index') }}">
                            <i class="link-icon" data-feather="truck"></i>
                            <span class="link-title">Maklumat Kontraktor</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() == 'Bajet.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('Bajet.index') }}">
                            <i class="link-icon" data-feather="dollar-sign"></i>
                            <span class="link-title">Bajet</span>
                        </a>
                    </li>
                {{-- @endif --}}

                {{-- <li class="nav-item nav-category">Pengurusan Sumbangan</li>
                <li class="nav-item {{ (\Request::route()->getName() == 'permohonan-pengurusan-sumbangan.index') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('permohonan-pengurusan-sumbangan.index') }}">
                        <i class="link-icon" data-feather="dollar-sign"></i>
                        <span class="link-title">Permohonan Sumbangan</span>
                    </a>
                </li>
                <li class="nav-item {{ (\Request::route()->getName() == 'kerani-pengurusan-sumbangan.index') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('kerani-pengurusan-sumbangan.index') }}">
                        <i class="link-icon" data-feather="folder-minus"></i>
                        <span class="link-title">Kumpulan Sumbangan</span>
                    </a>
                </li>
                <li class="nav-item {{ (\Request::route()->getName() == 'mesyuarat-sumbangan.index') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('mesyuarat-sumbangan.index') }}">
                        <i class="link-icon" data-feather="calendar"></i>
                        <span class="link-title">Mesyuarat</span>
                    </a>
                </li> --}}



            {{-- @endif --}}
        </ul>
    </div>
  </nav>
