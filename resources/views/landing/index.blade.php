@extends('layout.landing_layout')

@section('content')
<div class="main-banner-wrapper">
    <section class="banner-style-one owl-theme owl-carousel">
        <div class="slide slide-one" style="background-image: url({{ asset('landing/images/slider/slider-1-1.jpg') }});">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <span class="tag-line">Welcome to Dexen Company</span>
                        <h3 class="banner-title">Trusted Printing <br> & Copy Center</h3>
                        <p>Design helps us to stand out, It tells a story <br> about us and what we stand for.
                        </p>
                        <div class="btn-block">
                            <a href="#" class="banner-btn">Order Online Now</a>
                        </div><!-- /.btn-block -->
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.slide -->
        <div class="slide slide-one" style="background-image: url({{ asset('landing/images/slider/slider-1-2.jpg') }});">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <span class="tag-line">Welcome to Dexen Company</span>
                        <h3 class="banner-title">Trusted Printing <br> & Copy Center</h3>
                        <p>Design helps us to stand out, It tells a story <br> about us and what we stand for.
                        </p>
                        <div class="btn-block">
                            <a href="#" class="banner-btn">Order Online Now</a>
                        </div><!-- /.btn-block -->
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.slide -->
        <div class="slide slide-one" style="background-image: url({{ asset('landing/images/slider/slider-1-1.jpg') }});">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <span class="tag-line">Welcome to Dexen Company</span>
                        <h3 class="banner-title">Trusted Printing <br> & Copy Center</h3>
                        <p>Design helps us to stand out, It tells a story <br> about us and what we stand for.
                        </p>
                        <div class="btn-block">
                            <a href="#" class="banner-btn">Order Online Now</a>
                        </div><!-- /.btn-block -->
                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.slide -->
    </section><!-- /.banner-style-one -->
    <div class="carousel-btn-block banner-carousel-btn">
        <span class="carousel-btn left-btn"><i class="dexen-icon-music-player-play"></i></span>
        <span class="carousel-btn right-btn"><i class="dexen-icon-music-player-play"></i></span>
    </div><!-- /.carousel-btn-block banner-carousel-btn -->
</div><!-- /.main-banner-wrapper -->
<section class="service-two sec-pad sec-pad-content-margin-30 thm-gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="block-title mb-0">
                    <p class="block-title__tag-line ">Our Services</p>
                    <h2 class="block-title__title">Dexen Printing and <br> Copy Centre Best in <br> Your Town
                    </h2>
                </div><!-- /.block-title -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 d-flex">
                <div class="my-auto">
                    <p class="service-two__title-text">Lorem ipsum is simply free text dolor sit amett
                        consectetur adipiscing elit. When an unknown printer took a galley of type and scrambled
                        it to make a type specimen book. It has survived not only five centuries. There are many
                        people variation of passages of lorem Ipsum available in the majority have suffer
                        alteration in some.</p>
                </div><!-- /.my-auto -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <div class="row-5-col">
            <div class="col-5-col">
                <div class="service-two__single hvr-float-shadow content-margin-30">
                    <img src="{{  asset('landing/images/icons/tee-1-1.png') }}" alt="Awesome Image" />
                    <h3 class="service-two__title">
                        <a href="single-service.html">T-Shirt Printing</a>
                    </h3><!-- /.service-two__title -->
                </div><!-- /.service-two__single -->
            </div><!-- /.col-5-col -->
            <div class="col-5-col">
                <div class="service-two__single hvr-float-shadow content-margin-30">
                    <img src="{{  asset('landing/images/icons/phone-1-1.png') }}" alt="Awesome Image" />
                    <h3 class="service-two__title">
                        <a href="single-service.html">Flyer Printing</a>
                    </h3><!-- /.service-two__title -->
                </div><!-- /.service-two__single -->
            </div><!-- /.col-5-col -->
            <div class="col-5-col">
                <div class="service-two__single hvr-float-shadow content-margin-30">
                    <img src="{{  asset('landing/images/icons/sticker-1-1.png') }}" alt="Awesome Image" />
                    <h3 class="service-two__title">
                        <a href="single-service.html">Sticker Printing</a>
                    </h3><!-- /.service-two__title -->
                </div><!-- /.service-two__single -->
            </div><!-- /.col-5-col -->
            <div class="col-5-col">
                <div class="service-two__single hvr-float-shadow content-margin-30">
                    <img src="{{  asset('landing/images/icons/poster-1-1.png') }}" alt="Awesome Image" />
                    <h3 class="service-two__title">
                        <a href="single-service.html">Poster Printing</a>
                    </h3><!-- /.service-two__title -->
                </div><!-- /.service-two__single -->
            </div><!-- /.col-5-col -->
            <div class="col-5-col">
                <div class="service-two__single hvr-float-shadow content-margin-30">
                    <img src="{{  asset('landing/images/icons/postcard-1-1.png') }}" alt="Awesome Image" />
                    <h3 class="service-two__title">
                        <a href="single-service.html">Postcards Printing</a>
                    </h3><!-- /.service-two__title -->
                </div><!-- /.service-two__single -->
            </div><!-- /.col-5-col -->
        </div><!-- /.row-5-col -->
    </div><!-- /.container -->
</section><!-- /.service-two -->
<section class="service-one sec-pad-top">
    <div class="container">
        <div class="block-title text-center">
            <p class="block-title__tag-line ">Our Features</p>
            <h2 class="block-title__title">What We Can Do</h2>
        </div><!-- /.block-title -->
        <div class="row">
            <div class="col-lg-4">
                <div class="service-one__single">
                    <div class="service-one__image">
                        <img src="{{ asset('landing/images/service/service-1-1.jpg') }}" alt="Awesome Image" />
                    </div><!-- /.service-one__image -->
                    <div class="service-one__text-block">
                        <h3 class="service-one__title"><a href="single-service.html">3D Printing</a></h3>
                        <!-- /.service-one__title -->
                        <p class="service-one__text">There are many people variation of pass of lorem sum
                            available inthe majority have suffer freedom text some.</p>
                        <!-- /.service-one__text -->
                        <a href="#" class="service-one__link"><i class="fa fa-angle-right"></i></a>
                    </div><!-- /.service-one__text-block -->
                </div><!-- /.service-one__single -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="service-one__single">
                    <div class="service-one__image">
                        <img src="{{ asset('landing/images/service/service-1-2.jpg') }}" alt="Awesome Image" />
                    </div><!-- /.service-one__image -->
                    <div class="service-one__text-block">
                        <h3 class="service-one__title"><a href="single-service.html">Digital Printing</a></h3>
                        <!-- /.service-one__title -->
                        <p class="service-one__text">There are many people variation of pass of lorem sum
                            available inthe majority have suffer freedom text some.</p>
                        <!-- /.service-one__text -->
                        <a href="#" class="service-one__link"><i class="fa fa-angle-right"></i></a>
                    </div><!-- /.service-one__text-block -->
                </div><!-- /.service-one__single -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="service-one__single">
                    <div class="service-one__image">
                        <img src="{{ asset('landing/images/service/service-1-3.jpg') }}" alt="Awesome Image" />
                    </div><!-- /.service-one__image -->
                    <div class="service-one__text-block">
                        <h3 class="service-one__title"><a href="single-service.html">Offset Printing</a></h3>
                        <!-- /.service-one__title -->
                        <p class="service-one__text">There are many people variation of pass of lorem sum
                            available inthe majority have suffer freedom text some.</p>
                        <!-- /.service-one__text -->
                        <a href="#" class="service-one__link"><i class="fa fa-angle-right"></i></a>
                    </div><!-- /.service-one__text-block -->
                </div><!-- /.service-one__single -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.service-one -->
<section class="testimonial-one">
    <div class="container">
        <div class="testimonial-one__carousel owl-theme owl-carousel">
            <div class="item">
                <div class="testimonial-one__single">
                    <div class="testimonial-one__image">
                        <img src="{{ asset('landing/images/testimonials/testimonials-1-1.jpg') }}" alt="Awesome Image" />
                    </div><!-- /.testimonial-one__image -->
                    <p class="testimonial-one__text">This is due to their excellent service, competitive pricing
                        and customer support. It’s throughly refresing to get such a personal touch.</p>
                    <!-- /.testimonial-one__text -->
                    <h4 class="testimonial-one__name">Christine Eve</h4>
                </div><!-- /.testimonial-one__single -->
            </div><!-- /.item -->
            <div class="item">
                <div class="testimonial-one__single">
                    <div class="testimonial-one__image">
                        <img src="{{ asset('landing/images/testimonials/testimonials-1-2.jpg') }}" alt="Awesome Image" />
                    </div><!-- /.testimonial-one__image -->
                    <p class="testimonial-one__text">This is due to their excellent service, competitive pricing
                        and customer support. It’s throughly refresing to get such a personal touch.</p>
                    <!-- /.testimonial-one__text -->
                    <h4 class="testimonial-one__name">Marilynn Charette</h4>
                </div><!-- /.testimonial-one__single -->
            </div><!-- /.item -->
            <div class="item">
                <div class="testimonial-one__single">
                    <div class="testimonial-one__image">
                        <img src="{{ asset('landing/images/testimonials/testimonials-1-3.jpg') }}" alt="Awesome Image" />
                    </div><!-- /.testimonial-one__image -->
                    <p class="testimonial-one__text">This is due to their excellent service, competitive pricing
                        and customer support. It’s throughly refresing to get such a personal touch.</p>
                    <!-- /.testimonial-one__text -->
                    <h4 class="testimonial-one__name">Mikaela Cunniffe</h4>
                </div><!-- /.testimonial-one__single -->
            </div><!-- /.item -->
        </div><!-- /.testimonial-one__carousel -->
    </div><!-- /.container -->
</section><!-- /.testimonial-one -->
<section class="brand-one">
    <div class="container">
        <div class="brand-one__carousel owl-carousel owl-theme">
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-1.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-2.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-3.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-4.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-5.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-1.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-2.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-3.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-4.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-5.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-1.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-2.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-3.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-4.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
            <div class="item">
                <img src="{{  asset('landing/images/brand/brand-1-5.png') }}" alt="Awesome Image" />
            </div><!-- /.item -->
        </div><!-- /.brand-one__carousel -->
    </div><!-- /.container -->
</section><!-- /.brand-one -->
<section class="cta-two">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex">
                <div class="cta-two__content my-auto">
                    <div class="block-title">
                        <p class="block-title__tag-line block-title__light-color">Stationery Printing</p>
                        <h2 class="block-title__title block-title__light-color">Order for Business <br> Stuff
                        </h2>
                    </div><!-- /.block-title -->
                    <ul class="cta-two__features">
                        <li class="cta-two__features-item "><i class="fa fa-check"></i>Professional designs with
                            added fizz</li>
                        <li class="cta-two__features-item "><i class="fa fa-check"></i>Create an army of
                            business stationery</li>
                        <li class="cta-two__features-item "><i class="fa fa-check"></i>Take your attention to
                            detail up a level</li>
                        <li class="cta-two__features-item "><i class="fa fa-check"></i>Totally safe for laser
                            printers </li>
                    </ul><!-- /.cta-two__features -->
                    <a href="#" class="thm-btn cta-two__btn">Learn More</a>
                </div><!-- /.cta-two__content -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <img src="{{ asset('landing/images/resources/cta-1-2.png') }}" alt="Awesome Image" class="float-left" />
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.cta-two -->
<section class="about-one sec-pad-top">
    <div class="container">
        <div class="block-title text-center">
            <p class="block-title__tag-line ">Our Introduction</p>
            <h2 class="block-title__title">About Dexen</h2>
        </div><!-- /.block-title -->
        <div class="row">
            <div class="col-lg-4">
                <div class="about-one__single content-margin-60">
                    <h3 class="about-one__title"><a href="#">Best Printing Services</a></h3>
                    <p class="about-one__text">Phaseus site amet tristique ligua donec iaculis leo sus cipit.
                    </p>
                </div><!-- /.about-one__single -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="about-one__single content-margin-60">
                    <h3 class="about-one__title"><a href="#">Trusted & Secure</a></h3>
                    <p class="about-one__text">Phaseus site amet tristique ligua donec iaculis leo sus cipit.
                    </p>
                </div><!-- /.about-one__single -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="about-one__single content-margin-60">
                    <h3 class="about-one__title"><a href="#">Customer Satisfaction</a></h3>
                    <p class="about-one__text">Phaseus site amet tristique ligua donec iaculis leo sus cipit.
                    </p>
                </div><!-- /.about-one__single -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-one -->
<section class="price-one sec-pad sec-pad-content-margin-30 thm-gray-bg">
    <div class="container">
        <div class="block-title text-center">
            <p class="block-title__tag-line ">Choose Your Plan</p>
            <h2 class="block-title__title">Plans & Pricing</h2>
        </div><!-- /.block-title -->
        <div class="row">
            <div class="col-lg-4">
                <div class="price-one__single text-center content-margin-30">
                    <div class="price-one__top">
                        <p class="price-one__title">Standard</p><!-- /.price-one__title -->
                        <h3 class="price-one__price">$29.00</h3><!-- /.price-one__price -->
                    </div><!-- /.price-one__top -->
                    <div class="price-one__bottom">
                        <ul class="price-one__feature">
                            <li class="price-one__feature-item">Rounded & Colored
                            <li class="price-one__feature-item">Preimum Paper</li>
                            <li class="price-one__feature-item">Design Work</li>
                            <li class="price-one__feature-item">Extra Thick</li>
                        </ul><!-- /.price-one__feature -->
                        <a href="#" class="thm-btn">Select Plan</a>
                    </div><!-- /.price-one__bottom -->
                </div><!-- /.price-one__single -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="price-one__single text-center content-margin-30">
                    <div class="price-one__top">
                        <p class="price-one__title">Premium</p><!-- /.price-one__title -->
                        <h3 class="price-one__price">$39.00</h3><!-- /.price-one__price -->
                    </div><!-- /.price-one__top -->
                    <div class="price-one__bottom">
                        <ul class="price-one__feature">
                            <li class="price-one__feature-item">Rounded & Colored
                            <li class="price-one__feature-item">Preimum Paper</li>
                            <li class="price-one__feature-item">Design Work</li>
                            <li class="price-one__feature-item">Extra Thick</li>
                        </ul><!-- /.price-one__feature -->
                        <a href="#" class="thm-btn">Select Plan</a>
                    </div><!-- /.price-one__bottom -->
                </div><!-- /.price-one__single -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="price-one__single text-center content-margin-30">
                    <div class="price-one__top">
                        <p class="price-one__title">Ultimate</p><!-- /.price-one__title -->
                        <h3 class="price-one__price">$49.00</h3><!-- /.price-one__price -->
                    </div><!-- /.price-one__top -->
                    <div class="price-one__bottom">
                        <ul class="price-one__feature">
                            <li class="price-one__feature-item">Rounded & Colored
                            <li class="price-one__feature-item">Preimum Paper</li>
                            <li class="price-one__feature-item">Design Work</li>
                            <li class="price-one__feature-item">Extra Thick</li>
                        </ul><!-- /.price-one__feature -->
                        <a href="#" class="thm-btn">Select Plan</a>
                    </div><!-- /.price-one__bottom -->
                </div><!-- /.price-one__single -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.price-one -->
<section class="cta-one thm-black-bg">
    <div class="container-fluid p-0">
        <div class="row flex-row-reverse">
            <div class="col-lg-6 d-flex">
                <div class="cta-one__content-block my-auto">
                    <div class="block-title">
                        <p class="block-title__tag-line block-title__primary-color">Order Now</p>
                        <h2 class="block-title__title block-title__light-color">Get a Free Sample <br> to Feel
                            Colors</h2>
                    </div><!-- /.block-title -->
                    <p class="cta-one__text">Order a sample so you can touch and feel our premium range of <br>
                        papers and finishes for yourself. It’s free! Print full color on both <br> sides of your
                        business cards.</p>
                    <a href="contact.html" class="thm-btn thm-btn__base-bg cta-one__btn">Contact Us</a>
                </div><!-- /.cta-one__content-block -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="cta-one__image-block">
                    <img src="{{ asset('landing/images/resources/cta-1-1.jpg') }}" alt="Awesome Image" />
                    <div class="cta-one__image-block__content text-center">
                        <h3>Printfinity Makes Every Business Card <br>
                            Unique & Professional</h3>
                    </div><!-- /.cta-one__image-block__content -->
                </div><!-- /.cta-one__image-block -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.cta-one -->
<section class="project-style-one sec-pad sec-pad-content-margin-80">
    <div class="container">
        <div class="block-title text-center">
            <p class="block-title__tag-line">Work Showcase</p>
            <h2 class="block-title__title">Recent Projects</h2>
        </div><!-- /.block-title -->
        <div class="row">
            <div class="col-lg-4">
                <div class="project-style-one__single content-margin-80">
                    <div class="project-style-one__image-block">
                        <img src="{{ asset('landing/images/projects/project-1-1.jpg') }}" alt="Awesome Image" />
                    </div><!-- /.project-style-one__image-block -->
                    <div class="project-style-one__text-block thm-gray-bg text-center">
                        <span class="project-style-one__category">Printing</span>
                        <h3 class="project-style-one__title"><a href="#">Thick Paper Book</a></h3>
                        <!-- /.project-style-one__title -->
                        <a href="#" class="project-style-one__more-link"><i class="fa fa-plus"></i></a>
                    </div><!-- /.project-style-one__text-block -->
                </div><!-- /.project-style-one__single -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="project-style-one__single content-margin-80">
                    <div class="project-style-one__image-block">
                        <img src="{{ asset('landing/images/projects/project-1-2.jpg') }}" alt="Awesome Image" />
                    </div><!-- /.project-style-one__image-block -->
                    <div class="project-style-one__text-block thm-gray-bg text-center">
                        <span class="project-style-one__category">Copying</span>
                        <h3 class="project-style-one__title"><a href="#">Ninety Nine You</a></h3>
                        <!-- /.project-style-one__title -->
                        <a href="#" class="project-style-one__more-link"><i class="fa fa-plus"></i></a>
                    </div><!-- /.project-style-one__text-block -->
                </div><!-- /.project-style-one__single -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="project-style-one__single content-margin-80">
                    <div class="project-style-one__image-block">
                        <img src="{{ asset('landing/images/projects/project-1-3.jpg') }}" alt="Awesome Image" />
                    </div><!-- /.project-style-one__image-block -->
                    <div class="project-style-one__text-block thm-gray-bg text-center">
                        <span class="project-style-one__category">Printing</span>
                        <h3 class="project-style-one__title"><a href="#">Colorful Photo Print</a></h3>
                        <!-- /.project-style-one__title -->
                        <a href="#" class="project-style-one__more-link"><i class="fa fa-plus"></i></a>
                    </div><!-- /.project-style-one__text-block -->
                </div><!-- /.project-style-one__single -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.project-style-one -->
<div class="google-map" id="home-google-map">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4562.753041141002!2d-118.80123790098536!3d34.152323469614075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80e82469c2162619%3A0xba03efb7998eef6d!2sCostco+Wholesale!5e0!3m2!1sbn!2sbd!4v1562518641290!5m2!1sbn!2sbd"
        class="google-map__contact" allowfullscreen></iframe>
</div>
@endsection
