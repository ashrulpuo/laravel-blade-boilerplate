<div>
    <!-- Act only according to that maxim whereby you can, at the same time, will that it should become a universal law. - Immanuel Kant -->
    @if(isset($type))
    <div class="alert alert-custom alert-{{ $type }} fade show" id="success" role="alert">
        <div class="alert-icon"><i class="{{ $icon }}"></i></div>
        <div class="alert-text">
            @if(is_array($message) || is_object($message))
            <ul>
                @foreach ($message as $msg)
                <li>{{ $msg }}</li>
                @endforeach
            </ul>
            @else
            {!! $message !!}
            @endif
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
    @else
    <!-- using js -->
    <div id="alert" class="alert alert-custom alert-success fade show alert" style="display:none" role="alert">
        <div class="alert-icon"><i id="icon" class="flaticon2-check-mark"></i></div>
        <div class="alert-text" id="text"></div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
@endif
</div>
