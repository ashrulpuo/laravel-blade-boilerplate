<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modelHeading">Modal Title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="btn-close"></button>
            </div>
            </br>

            <div class="modal-body">
                <div id="danger" class="col-md-12 row" style="display:none;margin: auto;"></div>

                {{ $slot }}

            </div>

            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tutup</button>
                <button type="button" id="simpan" class="btn btn-success font-weight-bold">SIMPAN</button>
            </div>
        </div>
    </div>
</div>
