@extends('layout.app')

@section('css')
@endsection

@section('content')
<nav class='page-breadcrumb'>
    <ol class='breadcrumb'>
        <li class='breadcrumb-item'>Tetapan</li>
        <li class='breadcrumb-item active' aria-current='page'>Tetapan Pengguna</li>
    </ol>
</nav>

<div class='row'>
    <div class='col-lg-12'>
        @include('common.alert')

        <div class='card'>
            <div class='card-header'>
                <h4 class='card-title'>Tetapan Pengguna</h4>
                <p class='card-title-desc'>Senarai Pengguna</p>
                <div class='col'>
                    <div class='text-end'>
                        <a href="{{ route('Pengguna.create') }}" type='button' class='btn btn-inverse-primary'>Tambah</a>
                    </div>
                </div>
            </div>
            <div class='card-body'>
                <div class='col-sm-12 table-responsive'>
                    <table class='table dataTable no-footer' id='kt_datatable' data-route='{{ route('Pengguna.index') }}'></table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src='{{ asset('js/tetapan/Pengguna.js') }}'></script>
<script>
    function numOnly(selector){
        selector.value = selector.value.replace(/[^0-9]/g,'');
    }
</script>
@endsection

