<form class="form" action="{{ route('profil.updateProfile', $Pengguna->PenggunaId) }}" method="POST" id="tetapanPengguna">
    @csrf
    @method('PUT')

    <input type="hidden" name="PenggunaId" value="{{ $Pengguna->PenggunaId }}">
    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">Nama</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" value="{{ $Pengguna->name ?? '' }}" placeholder="Sila masukkan" />
        </div>
        @error('name')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">Emel</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="email" value="{{ $Pengguna->email ?? '' }}" name="email" placeholder="Sila masukkan" readonly/>
            @error('email')
                <span class="" style="color: red;">{{ $message }}</span>
            @enderror
        </div>

    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">No Telefon</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="NoTel" onkeyup="numOnly(this)" value="{{ $Pengguna->MaklumatPengguna->NoTel ?? '' }}" onblur="numOnly(this)" name="NoTel" placeholder="Sila masukkan" />
        </div>
        @error('NoTel')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">No Tel Pejabat</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="NoTelPejabat" onkeyup="numOnly(this)" value="{{ $Pengguna->MaklumatPengguna->NoTelPejabat ?? '' }}" onblur="numOnly(this)" name="NoTelPejabat" placeholder="Sila masukkan" />
        </div>
        @error('NoTelPejabat')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">Bahagian/ Seksyen</label>
        <div class="col-sm-9">
            <select class="form-select select2" name="RefBahagianId" id="RefBahagianId">
                <option value="" selected>Sila Pilih</option>
                @foreach ($RefBahagian as $bahagian)
                    <option value="{{ $bahagian->RefBahagianId }}"  {{ ( $Pengguna->MaklumatPengguna->BahagianId == $bahagian->RefBahagianId) ? 'selected' : '' }}>{{ $bahagian->Penerangan }}</option>
                @endforeach
            </select>
        </div>
        @error('RefBahagianId')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">Jawatan</label>
        <div class="col-sm-9">
            <select class="form-select select2" name="JawatanId" id="JawatanId">
                <option value="" selected>Sila Pilih</option>
                @foreach ($RefJawatan as $Jawatan)
                    <option value="{{ $Jawatan->RefJawatanId }}" {{ ( $Pengguna->MaklumatPengguna->JawatanId == $Jawatan->RefJawatanId) ? 'selected' : '' }}>{{ $Jawatan->Penerangan }}</option>
                @endforeach
            </select>
        </div>
        @error('JawatanId')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>
    <hr>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">KataLaluan</label>
        <div class="col-sm-9">
            <input type="password" class="form-control" name="password" placeholder="Sila masukkan" />
        </div>
    </div>

    <div class="card-footer bg-transparent">
        <div class="form-group row">
            <div class="col"></div>
            <div class="col-auto">
                <button type="submit" class="btn btn-success btn-xs">KEMASKINI</button>
            </div>
        </div>
    </div>
</form>
