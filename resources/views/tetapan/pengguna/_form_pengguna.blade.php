<form class="form" action="{{ route('Pengguna.update', $Pengguna->PenggunaId) }}" method="POST" id="tetapanPengguna">
    @csrf
    @method('PUT')

    <input type="hidden" name="PenggunaId" value="{{ $Pengguna->PenggunaId }}">
    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">Nama</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" value="{{ $Pengguna->name ?? '' }}" placeholder="Sila masukkan" />
        </div>
        @error('name')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">Emel</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="email" value="{{ $Pengguna->email ?? '' }}" name="email" placeholder="Sila masukkan" />
            @error('email')
                <span class="" style="color: red;">{{ $message }}</span>
            @enderror
        </div>

    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">No Telefon</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="NoTel" onkeyup="numOnly(this)" value="{{ $Pengguna->MaklumatPengguna->NoTel ?? '' }}" onblur="numOnly(this)" name="NoTel" placeholder="Sila masukkan" />
        </div>
        @error('NoTel')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">No Tel Pejabat</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="NoTelPejabat" onkeyup="numOnly(this)" value="{{ $Pengguna->MaklumatPengguna->NoTelPejabat ?? '' }}" onblur="numOnly(this)" name="NoTelPejabat" placeholder="Sila masukkan" />
        </div>
        @error('NoTelPejabat')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">Bahagian/ Seksyen</label>
        <div class="col-sm-9">
            <select class="form-select select2" name="RefBahagianId" id="RefBahagianId">
                <option value="" selected>Sila Pilih</option>
                @foreach ($RefBahagian as $bahagian)
                    <option value="{{ $bahagian->RefBahagianId }}"  {{ ( $Pengguna->MaklumatPengguna->BahagianId == $bahagian->RefBahagianId) ? 'selected' : '' }}>{{ $bahagian->Penerangan }}</option>
                @endforeach
            </select>
        </div>
        @error('RefBahagianId')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">Jawatan</label>
        <div class="col-sm-9">
            <select class="form-select select2" name="JawatanId" id="JawatanId">
                <option value="" selected>Sila Pilih</option>
                @foreach ($RefJawatan as $Jawatan)
                    <option value="{{ $Jawatan->RefJawatanId }}" {{ ( $Pengguna->MaklumatPengguna->JawatanId == $Jawatan->RefJawatanId) ? 'selected' : '' }}>{{ $Jawatan->Penerangan }}</option>
                @endforeach
            </select>
        </div>
        @error('JawatanId')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>

    <div class="row mb-3">
        <label class="col-sm-3 col-form-label">Role</label>
        <div class="col-sm-9">
            <select class="js-example-basic-multiple form-select select2" multiple="multiple" id="RoleId" name="RoleId[]" data-width="100%">
                @foreach ($Role as $role)
                    <option value="{{ $role->RoleId }}" {{ (in_array($role->RoleId, $selectedRole)) ? 'selected' : '' }}>{{ $role->Role }}</option>
                @endforeach
            </select>
        </div>
        @error('RoleId')
            <span class="" style="color: red;">{{ $message }}</span>
        @enderror
    </div>
    <div class="row mb-3">
        <label class="col-sm-3 col-form-label"></label>
        <div class="col-sm-9">
            <input type="checkbox" class="form-check-input" name="Penyiasat" value="10" {{ ( $Pengguna->MaklumatPengguna->Penyiasat == 10) ? 'checked' : '' }}>
            <label class="form-check-label">
                Penyiasat
            </label>
        </div>
    </div>

    <div class="card-footer bg-transparent">
        <div class="form-group row">
            <div class="col">
                <a href="{{ route('Pengguna.index') }}" type="button" class="btn btn-danger btn-xs">KEMBALI KE SENARAI</a>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-success btn-xs">HANTAR</button>
            </div>
        </div>
    </div>
</form>
