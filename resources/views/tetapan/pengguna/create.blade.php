@extends('layout.app')

@section('content')
<nav class='page-breadcrumb'>
    <ol class='breadcrumb'>
        <li class='breadcrumb-item'>Tetapan</li>
        <li class='breadcrumb-item active' aria-current='page'>Tetapan Pengguna</li>
    </ol>
</nav>

<div class='row'>
    <div class='col-lg-12'>
        @include('common.alert')

        <div class='card'>
            <div class='card-header'>
                <h4 class='card-title'>Tetapan Pengguna</h4>
                <p class='card-title-desc'>Daftar Pengguna</p>
            </div>
            <div class='card-body'>
                <form class="form" action="{{ route('Pengguna.store') }}" method="POST" id="tetapanPengguna">
                    @csrf

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" name="name" value="" placeholder="Sila masukkan" />
                            @error('name')
                                <span class="" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label">Emel</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="email" value="" name="email" placeholder="Sila masukkan" />
                            @error('email')
                                <span class="" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label">No Telefon</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="NoTel" onkeyup="numOnly(this)" value="" onblur="numOnly(this)" name="NoTel" placeholder="Sila masukkan" />
                            @error('NoTel')
                                <span class="" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label">No Tel Pejabat</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="NoTelPejabat" onkeyup="numOnly(this)" value="" onblur="numOnly(this)" name="NoTelPejabat" placeholder="Sila masukkan" />
                            @error('NoTelPejabat')
                                <span class="" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label">Bahagian/ Seksyen</label>
                        <div class="col-sm-9">
                            <select class="form-select select2" name="RefBahagianId" id="RefBahagianId">
                                <option value="" selected>Sila Pilih</option>
                                @foreach ($RefBahagian as $bahagian)
                                    <option value="{{ $bahagian->RefBahagianId }}">{{ $bahagian->Penerangan }}</option>
                                @endforeach
                            </select>
                            @error('RefBahagianId')
                                <span class="" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label">Jawatan</label>
                        <div class="col-sm-9">
                            <select class="form-select select2" name="JawatanId" id="JawatanId">
                                <option value="" selected>Sila Pilih</option>
                                @foreach ($RefJawatan as $Jawatan)
                                    <option value="{{ $Jawatan->RefJawatanId }}" >{{ $Jawatan->Penerangan }}</option>
                                @endforeach
                            </select>
                            @error('JawatanId')
                                <span class="" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label">Role</label>
                        <div class="col-sm-9">
                            <select class="js-example-basic-multiple form-select select2" multiple="multiple" id="RoleId" name="RoleId[]" data-width="100%">
                                @foreach ($Role as $role)
                                    <option value="{{ $role->RoleId }}">{{ $role->Role }}</option>
                                @endforeach
                            </select>
                            @error('RoleId')
                                <span class="" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-9">
                            <input type="checkbox" class="form-check-input" name="Penyiasat" value="10">
                            <label class="form-check-label">
                                Penyiasat
                            </label>
                        </div>
                    </div>

                    <div class="card-footer bg-transparent">
                        <div class="form-group row">
                            <div class="col">
                                <a href="{{ route('Pengguna.index') }}" type="button" class="btn btn-danger btn-xs">KEMBALI KE SENARAI</a>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-success btn-xs">HANTAR</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
// A $( document ).ready() block.
$( document ).ready(function() {
    if ($(".js-example-basic-multiple").length) {
        $(".js-example-basic-multiple").select2();
    }

    function numOnly(selector){
        selector.value = selector.value.replace(/[^0-9]/g,'');
    }
});
</script>
@endsection

