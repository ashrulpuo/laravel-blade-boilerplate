@extends('layout.app')

@section('content')
<nav class='page-breadcrumb'>
    <ol class='breadcrumb'>
        <li class='breadcrumb-item'>Tetapan</li>
        <li class='breadcrumb-item active' aria-current='page'>Tetapan Pengguna</li>
    </ol>
</nav>

<div class='row'>
    <div class='col-lg-12'>
        @include('common.alert')

        <div class='card'>
            <div class='card-header'>
                <h4 class='card-title'>Tetapan Kemaskini Pengguna</h4>
                <p class='card-title-desc'>Kemaskini Pengguna</p>
            </div>
            <div class='card-body'>
                @include('tetapan.pengguna._form_pengguna')
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
// A $( document ).ready() block.
$( document ).ready(function() {
    if ($(".js-example-basic-multiple").length) {
        $(".js-example-basic-multiple").select2();
    }

    function numOnly(selector){
        selector.value = selector.value.replace(/[^0-9]/g,'');
    }
});
</script>
@endsection

