@extends('layout.app')

@section('content')
<nav class='page-breadcrumb'>
    <ol class='breadcrumb'>
        <li class='breadcrumb-item'>Tetapan</li>
        <li class='breadcrumb-item active' aria-current='page'>Tetapan Module</li>
    </ol>
</nav>

<div class='row'>
    <div class='col-lg-12'>
        @include('common.alert')

        <div class='card'>
            <div class='card-header'>
                <h4 class='card-title'>Tetapan Module</h4>
                <p class='card-title-desc'>Senarai Module</p>
                <div class='col'>
                    <div class='text-end'>
                        <button type='button' class='btn btn-inverse-primary create'>Tambah</button>
                    </div>
                </div>
            </div>
            <div class='card-body'>
                <div class='col-sm-12 table-responsive'>
                    <table class='table dataTable no-footer' id='kt_datatable' data-route='{{ route('Module.index') }}'></table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modalRoute modal fade" id="modalRoute" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Route</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="btn-close"></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class='table dataTable no-footer' id='route-dt' style="width: 100%;" data-route='{{ route('route-list', ':id') }}'></table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modalDeleteRoute modal fade" id="modalDeleteRoute" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Route</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="btn-close"></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class='table dataTable no-footer' id='hapus-route-dt' style="width: 100%;" data-route='{{ route('hapus-route-list', ':id') }}'></table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>


<x-modal title="Custom Title" id="modal">
    @include('tetapan.module._form_module')
 @endComponentClass
@endsection



@section('js')
<script src='{{ asset('js/tetapan/Module.js') }}'></script>
@endsection

