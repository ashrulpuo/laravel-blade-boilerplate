<form class="form" method="POST" id="tetapanRoutes">
    @csrf

	<input type="text" hidden class="form-control" id="RoutesId" name="RoutesId"  /> 
	<div class="form-group">
        <label>Method</label>
        <input type="text" class="form-control" id="Method" name="Method" placeholder="Sila masukkan" />
    </div> 
	<div class="form-group">
        <label>Uri</label>
        <input type="text" class="form-control" id="Uri" name="Uri" placeholder="Sila masukkan" />
    </div> 
	<div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" id="Name" name="Name" placeholder="Sila masukkan" />
    </div> 
	<div class="form-group">
        <label>Controller</label>
        <input type="text" class="form-control" id="Controller" name="Controller" placeholder="Sila masukkan" />
    </div> 
	<div class="form-group">
        <label>Function</label>
        <input type="text" class="form-control" id="Function" name="Function" placeholder="Sila masukkan" />
    </div> 
	<div class="form-group">
        <label>Namespace</label>
        <input type="text" class="form-control" id="Namespace" name="Namespace" placeholder="Sila masukkan" />
    </div> 
</form>
