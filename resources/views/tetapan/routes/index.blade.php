@extends('layout.app')

@section('content')
<nav class='page-breadcrumb'>
    <ol class='breadcrumb'>
        <li class='breadcrumb-item'>Tetapan</li>
        <li class='breadcrumb-item active' aria-current='page'>Tetapan Routes</li>
    </ol>
</nav>

<div class='row'>
    <div class='col-lg-12'>
        @include('common.alert')

        <div class='card'>
            <div class='card-header'>
                <h4 class='card-title'>Tetapan Routes</h4>
                <p class='card-title-desc'>Senarai Routes</p>
                <div class='col'>
                    <div class='text-end'>
                        <button type='button' class='btn btn-inverse-primary create'>Tambah</button>
                    </div>
                </div>
            </div>
            <div class='card-body'>
                <div class='col-sm-12 table-responsive'>
                    <table class='table dataTable no-footer' id='kt_datatable' data-route='{{ route('Routes.index') }}'></table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src='{{ asset('js/tetapan/Routes.js') }}'></script>
@endsection

