@extends('layout.app')

@section('content')
<nav class='page-breadcrumb'>
    <ol class='breadcrumb'>
        <li class='breadcrumb-item'>Tetapan</li>
        <li class='breadcrumb-item active' aria-current='page'>Kemaskini Tetapan Role</li>
    </ol>
</nav>

<div class='row'>
    <div class='col-lg-12'>
        @include('common.alert')


        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-0">KEMASKINI ROLE</h4>
                    </div>
                    <form action="{{ route('Role.update', $role->RoleId) }}" method="post">
                        @method('PUT')
                        @csrf

                        <input type="hidden" name="RoleId" value="{{ $role->RoleId }}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div class="mb-3">
                                        <label for="name" class="form-label">Peranan</label>
                                        <input id="name" class="form-control" name="Role" value="{{ $role->Role }}" type="text">
                                        @error('Role')
                                            <span class="" style="color: red;">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <h4 class="card-title">Sila Tick Modul</h4>
                                    @forelse ($modules as $module)
                                        <div class="mb-3">
                                            <div class="form-check">
                                                <label class="form-check-label" for="termsCheck">
                                                    {{ $module->Module }}
                                                </label>
                                                <input type="checkbox" value={{ $module->ModuleId }} {{ (in_array($module->ModuleId, $selectedId)) ? 'checked' : '' }} class="form-check-input" name="module[]">
                                            </div>
                                        </div>
                                    @empty
                                        <p class="text-muted mb-3">Tiada Module Didaftarkan</p>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent">
                            <div class="form-group row">
                                <div class="col">
                                    <a href="{{ route('Role.index') }}" type="button" class="btn btn-danger btn-xs">KEMBALI KE SENARAI</a>
                                </div>
                                <div class="col-auto">
                                    <button type="submit" class="btn btn-success btn-xs">HANTAR</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')

@endsection

