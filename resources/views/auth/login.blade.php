<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Log Masuk | Zakat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesdesign" name="author" />
    <!-- App favicon -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">

    <link href="{{ asset('asset/login/login.bundle.css') }}" rel="stylesheet">
    <style>
        .authentication-bg {
            background-size: cover;
            background-repeat: no-repeat;
            background-image: url("../images/login-img.png");
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
        }
    </style>
</head>

<body>
    <div class="main-wrapper">
        <div class="page-wrapper full-page">
            <div class="page-content d-flex align-items-center justify-content-center">

                <div class="row w-100 mx-0 auth-page">
                    <div class="card">
                        <div class="row">
                            <div class="col-md-4 pe-md-0">
                                <div class="auth-side-wrapper">

                                </div>
                            </div>
                            <div class="col-md-8 ps-md-0">
                                <div class="auth-form-wrapper px-4 py-5">
                                    <a href="#" class="noble-ui-logo d-block mb-2">Selamat Datang
                                        <span>!</span></a>
                                    <h5 class="text-muted fw-normal mb-4">Log masuk untuk ke dashboard.</h5>

                                    @if (session('status'))
                                        <div class="mb-4 font-medium text-sm text-green-600">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    <form class="forms-sample" method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="mb-3">
                                            <label for="userEmail" class="form-label">E-mel</label>
                                            <input type="email" class="form-control" name="email" id="email"
                                                placeholder="Masuk Email">
                                        </div>
                                        <div class="mb-3">
                                            <label for="userPassword" class="form-label">Kata Laluan</label>
                                            <div class="position-relative auth-pass-inputgroup input-custom-icon">
                                                <span class="bx bx-lock-alt"></span>
                                                <input type="password" class="form-control" name="password"
                                                    id="password-input" placeholder="Masuk Katalaluan">
                                                <button type="button"
                                                    class="btn btn-link position-absolute h-100 end-0 top-0"
                                                    id="password-addon">
                                                    <i class="mdi mdi-eye-outline font-size-18 text-muted"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="mt-3">
                                            <button class="btn btn-primary w-100 waves-effect waves-light"
                                                type="submit">Log Masuk</button>
                                        </div>
                                        <a href="{{ route('permohonan-akaun.index') }}" class="d-block mt-3 text-muted">Daftar Pengguna</a>
                                        <a href="{{ route('landing.index') }}" class="d-block mt-3 text-muted">Kembali</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
