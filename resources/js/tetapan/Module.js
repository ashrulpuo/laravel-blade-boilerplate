$(function() {

var statusCode;

var table = $('#kt_datatable')
var ModuleDT = table.DataTable({
    responsive: true,
    searchDelay: 500,
    processing: true,
    serverSide: true,
    scrollCollapse: true,
    pagingType: 'full_numbers',
    ordering: false,
    responsive: true,
    language: {
        search: "Carian :"
    },
    ajax: {
        url: table.data('route'),
        method: 'GET',
        dataType: 'json',
        dataSrc: "data",
    },
    columns: [
		{
            title: '#',
            data: 'ModuleId',
            render: function(data, type, full, meta) {
                return meta.row + 1;
            }
        },
		{
            title: 'Module',
            data: 'Module'
        },
		{
            title: 'Keterangan',
            data: 'Keterangan'
        },
        {
            title: 'Jumlah Route',
            data: 'routes_count'
        },
		{
            width: '75px',
            targets: -1,
            data: 'ModuleId',
            title: 'Tindakan',
            orderable: false,
            render: function(data, type, full, meta) {
                return `\
                    <a href="javascript:;" id="${full.ModuleId}" class="addRoute">\
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>\
                    </a>\
                    <a href="javascript:;" id="${full.ModuleId}" class="deleteRoute">\
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-delete"><path d="M21 4H8l-7 8 7 8h13a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path><line x1="18" y1="9" x2="12" y2="15"></line><line x1="12" y1="9" x2="18" y2="15"></line></svg>
                    </a>\
                    <a href="javascript:;" id="${full.ModuleId}" class="edit">\
                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg>\
                    </a>\
                    <a href="javascript:;" id="${full.ModuleId}" class="delete" style="color:red">\
                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2'><polyline points='3 6 5 6 21 6'></polyline><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg>\
                    </a>\
                `;
            },
        },
    ],
});

table.each(function() {
    var datatable = $(this);
    // SEARCH - Add the placeholder for Search and Turn this into in-line form control
    var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
    search_input.attr('placeholder', 'Search');
    search_input.removeClass('form-control-sm');
    // LENGTH - Inline-Form control
    var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
    length_sel.removeClass('form-control-sm');
});

$('body').on('click', '.create', function() {
    $('#tetapanModule')[0].reset();
    $('#modelHeading').html("Tetapan Module" );

    $('#modelHeading').html('Daftar');
    $('#simpan').html('Hantar');
    $('#modal').modal('show');
});

$('body').on('click', '.addRoute', function() {


    var routeTable = $('#route-dt')

    var id = $(this).attr('id');
    var url = routeTable.data('route');
    url = url.replace(':id', id);

    var routeTable = routeTable.DataTable({
        responsive: true,
        searchDelay: 500,
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        pagingType: 'full_numbers',
        ordering: false,
        responsive: true,
        language: {
            search: "Carian :"
        },
        ajax: {
            url: url,
            method: 'GET',
            dataType: 'json',
            dataSrc: "data",
        },
        columns: [
            {
                title: '#',
                data: 'RoutesId',
                render: function(data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                title: 'Method',
                data: 'Method'
            },
            {
                title: 'Name',
                data: 'Name'
            },
            {
                title: 'Controller',
                data: 'Controller'
            },
            {
                title: 'Function',
                data: 'Function'
            },
            {
                title: 'Hash',
                data: 'Hash'
            },
            {
                width: '75px',
                targets: -1,
                data: 'RoutesId',
                title: 'Tindakan',
                orderable: false,
                render: function(data, type, full, meta) {
                    return `\
                        <a href="javascript:;" id="${full.RoutesId}" data-module="${id}" class="tambahRouteModule">\
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>\
                        </a>\
                    `;
                },
            },
        ],
    });

    routeTable.each(function() {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });

    $('#modalRoute').modal('show');
});


$('body').on('click', '.deleteRoute', function() {


    var routeHapusTable = $('#hapus-route-dt')

    var id = $(this).attr('id');
    var url = routeHapusTable.data('route');
    url = url.replace(':id', id);

    var routeHapusTable = routeHapusTable.DataTable({
        responsive: true,
        searchDelay: 500,
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        pagingType: 'full_numbers',
        ordering: false,
        responsive: true,
        language: {
            search: "Carian :"
        },
        ajax: {
            url: url,
            method: 'GET',
            dataType: 'json',
            dataSrc: "data",
        },
        columns: [
            {
                title: '#',
                data: 'RoutesId',
                render: function(data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                title: 'Method',
                data: 'Method'
            },
            {
                title: 'Name',
                data: 'Name'
            },
            {
                title: 'Controller',
                data: 'Controller'
            },
            {
                title: 'Function',
                data: 'Function'
            },
            {
                title: 'Hash',
                data: 'Hash'
            },
            {
                width: '75px',
                targets: -1,
                data: 'RoutesId',
                title: 'Tindakan',
                orderable: false,
                render: function(data, type, full, meta) {
                    return `\
                        <a href="javascript:;" id="${full.RoutesId}" data-module="${id}" class="hapusRouteModule">\
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                        </a>\
                    `;
                },
            },
        ],
    });

    routeHapusTable.each(function() {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');
    });

    $('#modalDeleteRoute').modal('show');
});

$('#modalRoute').on('hidden.bs.modal', function () {
    $('#route-dt').DataTable().destroy();
})

$('#modalDeleteRoute').on('hidden.bs.modal', function () {
    $('#hapus-route-dt').DataTable().destroy();
})

$('body').on('click', '.tambahRouteModule', function() {
    var routeId = $(this).attr('id');
    var moduleId = $(this).attr('data-module');

    console.log(routeId);

    $.ajax({
        data: {'RouteId' : routeId, 'ModuleId' : moduleId},
        url: window.helper.url() + '/tetapan/add-module-route',
        type: 'POST',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: ((data) => {

            $('#route-dt').DataTable().draw();
        }),
        error: function(data) {
            window.helper.error(data)
        }
    });
});

$('body').on('click', '.hapusRouteModule', function() {
    var routeId = $(this).attr('id');
    var moduleId = $(this).attr('data-module');

    console.log(routeId);

    $.ajax({
        data: {'RouteId' : routeId, 'ModuleId' : moduleId},
        url: window.helper.url() + '/tetapan/hapus-module-route',
        type: 'POST',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: ((data) => {

            $('#hapus-route-dt').DataTable().draw();
        }),
        error: function(data) {
            window.helper.error(data)
        }
    });
});

//submit
$('#simpan').click(function(e) {
    // e.preventDefault();

    let method;
    let url;

    var id = $("#ModuleId").val();

    if (id === '' || id === undefined) {
        url = table.data('route');
        method = "POST";
    } else {
        url = table.data('route') + '/' + id;
        method = "PUT";
    }

    $.ajax({
        data: $('#tetapanModule').serialize(),
        url: url,
        type: method,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: ((data) => {
            statusCode = 1;
            window.helper.success(data, statusCode);
            ModuleDT.draw();
        }),
        error: function(data) {
            window.helper.error(data)
        }
    });
});

//edit
$('body').on('click', '.edit', function() {
    var ModuleId = $(this).attr('id');
    var route = table.data('route') + '/' + ModuleId + '/edit';

    $.get(route, function(data) {
		$('#ModuleId').val(data.data.ModuleId);
		$('#Module').val(data.data.Module);
		$('#Keterangan').val(data.data.Keterangan);

        $('#modelHeading').html("Kemaskini");
        $('#simpan').html("Kemaskini" );
        $('#modal').modal('show');
    })
});

//delete
$('body').on('click', '.delete', function() {
    var ModuleId = $(this).attr('id');
    var url = table.data('route') + '/' + ModuleId;

    Swal.fire({
        title: 'NOTIFIKASI',
        text: 'Adakah anda pasti?',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#038cfc',
        cancelButtonColor: '#999',
        confirmButtonText: 'YA',
        cancelButtonText: 'BATAL',
        allowOutsideClick: false
    }).then((response) => {
        if (response.value) {
              $.ajax({
                type: "DELETE",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    statusCode = 2;
                    window.helper.success(data, statusCode)
                    ModuleDT.draw();
                },
                error: function(data) {
                    $('#alert').removeClass('alert-success').addClass('alert-danger');
                    $('#icon').removeClass('flaticon2-check-mark').addClass('flaticon2-delete');
                    $("#text").html(data.message);
                    $(".alert").delay(3000).fadeOut();
                    ModuleDT.draw();
                }
            });
        }
    });
});
});
