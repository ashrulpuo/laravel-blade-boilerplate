$(function() {

var statusCode;

var table = $('#kt_datatable')

var RoutesDT = table.DataTable({
    responsive: true,
    searchDelay: 500,
    processing: true,
    serverSide: true,
    scrollCollapse: true,
    pagingType: 'full_numbers',
    ordering: false,
    responsive: true,
    language: {
        search: "Carian :"
    },
    ajax: {
        url: table.data('route'),
        method: 'GET',
        dataType: 'json',
        dataSrc: "data",
    },
    columns: [
		{
            title: '#',
            data: 'RoutesId',
            render: function(data, type, full, meta) {
                return meta.row + 1;
            }
        },
		{
            title: 'Method',
            data: 'Method'
        },
		{
            title: 'Uri',
            data: 'Uri'
        },
		{
            title: 'Name',
            data: 'Name'
        },
		{
            title: 'Controller',
            data: 'Controller'
        },
		{
            title: 'Function',
            data: 'Function'
        },
		{
            title: 'Namespace',
            data: 'Namespace'
        },
    ],
});

table.each(function() {
    var datatable = $(this);
    // SEARCH - Add the placeholder for Search and Turn this into in-line form control
    var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
    search_input.attr('placeholder', 'Search');
    search_input.removeClass('form-control-sm');
    // LENGTH - Inline-Form control
    var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
    length_sel.removeClass('form-control-sm');
});

$('body').on('click', '.create', function() {
    $.ajax({
        url: table.data('route'),
        type: 'POST',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: ((data) => {
            statusCode = 1;
            window.helper.success(data, statusCode);
            RoutesDT.draw();
        }),
        error: function(data) {
            window.helper.error(data)
        }
    });
});

});
