//error handling response
class Handler {
    url(){
        return window.location.origin;
    }
    success(data, statusCode) {
        $(".alert").css("display", "flex");
        $("#text").html(data.message)
        $(".alert").delay(3000).fadeOut();
        if (statusCode === 1) {
            $('#modal').modal('toggle');
        }
    }

    error(data) {
        var temp = "";
        $.each(data['responseJSON']['errors'], function(index, value) {

            temp += `
                    <div class="alert alert-danger" role="alert">
                        <i data-feather="alert-circle"></i>
                        ${data['responseJSON']['errors'][index]}
                    </div>
                `;

        })

        $('#danger').append(temp);
        $("#danger").css("display", "flex");
        $("#danger").delay(3000).fadeOut(400, function() {
            $('#danger').empty();
        });
    }
}

module.exports = new Handler()
